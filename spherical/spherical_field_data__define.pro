;+
;  NAME: spherical_field_data__define
;
;  PURPOSE:  
;    Creates a structure of type spherical_field_data, containing fields for
;    spherical vector field data, fieldline trajectories, and its associated
;    metadata.
;
;  INPUTS: none
;
;  OUTPUTS:
;    Defines a structure with the following fields:
;      *br: r-component of vector field, indexed (lon,lat,r)
;      *bth: theta-component of vector field, indexed (lon,lat,r)
;      *bph: phi-component of vector field, indexed (lon,lat,r)
;      *bderivs: pointer to a structure of pointers to derivative arrays
;      nr: number of gridpoints in r dimension
;      nlat: number of gridpoints in latitudinal (theta) dimension
;      nlon: number of gridpoints in longitudinal (phi) dimension
;      *rix: index array for r dimension
;      *lat: index array for meridional dimension, in degrees between [0,180]
;      *lon: index array for zonal dimension, in degrees between [0,360]
;      lonbounds=array of bounds [lonmin,lonmax], in degrees between 0 and
;                360, defining data that is bounded in longitude. The
;                convention is if lonbounds(0) is <0 or undefined, then it is
;                assumed that the data span all longidudes. Note that this
;                means that the default setting of lonbounds=[0,0] indicates
;                that the field is bounded, but since lonmin=lonmax this may
;                cause some problems with some routines.
;      *theta: index array for theta dimension, should be set to
;              (90-lat)*!dpi/180.
;      *phi: index array for phi dimension, should be set to lon*!dpi/180.
;      *str: n-element array for r coordinate of fieldline start points
;      *stth: n-element array for theta coordinate of fieldline start points
;      *stph: n-element array for phi coordinate of fieldline start points
;      *ptr: (n,stepmax)-element array of r coordinates of fieldlines
;      *ptth: (n,stepmax)-element array of theta coordinates of fieldlines
;      *ptph: (n,stepmax)-element array of phi coordinates of fieldlines
;      *nstep: n-element array of fieldline lengths
;      *extra_objects: array of extra objects added to the view for this field
;                      when rendered by the spherical_trackball_widget or
;                      spherical_draw_field routines 
; 
;  CALLING SEQUENCE:
;    data={spherical_field_data}
; 
;  NOTES:
;    1.  In the above, any field preceded by a * indicates that this field is
;        a pointer.
;    2.  In the above, n is the number of fieldlines, stepmax is the length in
;        points of the longest fieldline.
;    3.  The data needs to be on a longitude-latitude-radius grid, but the
;        grid spacing does not need to be uniform as long as the lon,lat, and
;        rix fields each increase monotonically.
; 
;  MODIFICATION HISTORY:
;    M.DeRosa - 13 Dec 2005 - created
;               24 Jan 2006 - added lonbounds tag
;               28 Jan 2011 - added bderivs tag
;               25 Apr 2011 - added extra_objects tag
;               23 Mar 2016 - corrected language in description above to
;                             indicate the consequences of lonbounds being
;                             automatically set to [0,0] upon the creation of
;                             this structure, in spite of the numeric values
;                             listed below
;
;-

pro spherical_field_data__define

;  dummy structure
dummy={spherical_field_data, $
  br:ptr_new(),bth:ptr_new(),bph:ptr_new(),bderivs:ptr_new(),$
  nr:-1l,nlat:-1l,nlon:-1l,$
  rix:ptr_new(),theta:ptr_new(),phi:ptr_new(),lat:ptr_new(),lon:ptr_new(),$
  lonbounds:[-1d,-1d],str:ptr_new(),stth:ptr_new(),stph:ptr_new(),$
  ptr:ptr_new(),ptth:ptr_new(),ptph:ptr_new(),nstep:ptr_new(),$
  extra_objects:ptr_new()}

end
