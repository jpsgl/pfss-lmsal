;+
;  NAME: spherical_image_erode
; 
;  PURPOSE:
;    This function erodes binary data mapped on a spherical surface using a
;    circular structural element (kernel).
; 
;  CALLING SEQUENCE:
;    result=spherical_image_erode(sph_image,radius) 
; 
;  INPUTS:
;    sph_image = a structure of type spherical_image_data (see
;                spherical_image_data__define.pro) with all fields defined.
;                If sph_image.image is not binary, then generate a binary
;                by doing a abs(*sph_image.image) lt 1.
;    radius = the radius of the circular erosion kernel in degrees
; 
;  OUTPUTS:
;    result = a structure of type spherical_image_data (see
;             spherical_image_data__define.pro) containing the eroded image.
;             This image is a binary image.
;
;  NOTES:
;    1.  Future improvement: allow arrays of structures and/or radii to be
;        passed into this routine.
;    2.  Another future improvement: allow routine to work on grayscale images
;        (i.e. assign to each point the minimum value of the surrounding
;        pixels defined by the kernel)
; 
;  MODIFICATION HISTORY:
;    M.DeRosa - 15 Dec 2005 - created
; 
;-

function spherical_image_erode,sph_image,radius

;  usage message
if n_elements(sph_image) eq 0 then begin
  print,'  ERROR in spherical_image_erode: no input data provided' 
  print,'  Calling sequence: result=spherical_image_erode(sph_image,radius)'
  return,{spherical_image_data}
endif

;  get radius in radians
if n_elements(radius) eq 0 then begin
  print,'  ERROR in spherical_image_erode: no erosion radius provided'
  return,{spherical_image_data}
endif
rad=radius(0)*!dpi/180
if rad le 0 then begin
  print,'  ERROR in spherical_image_erode: negative erosion radius' 
  return,{spherical_image_data}
endif

;  get a binary input image
imin=abs(*sph_image.image) lt 1

;  now dilate the background
sph_image2=sph_image
sph_image2.image=ptr_new(imin)
result=spherical_image_dilate(sph_image2,radius)

;  format result
sph_eroded=sph_image
sph_eroded.image=ptr_new(1b-*result.image)
return,sph_eroded

end
