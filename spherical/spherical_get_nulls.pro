;+
;  NAME: spherical_get_nulls.pro
;
;  PURPOSE:
;    Given vector field data on a spherical grid, this function will find the
;    locations of null points in the field using the trilinear method described
;    by Haynes & Parnell (2007,PhysPlas,14,082107).
;
;
;  CALLING SEQUENCE:
;    result = spherical_get_nulls(sph_data,rixrange=rixrange,nnulls=nnulls,
;      interpolates=interpolates,,/quiet,/debug)
;
;  INPUTS:
;    sph_data = a structure of type spherical_field_data (see
;      spherical_field_data__define.pro) with the following fields defined on
;      input: br,bth,bph,nlat,nlon,nr,rix,thix,phix,lat,lon.  Basically, one
;      needs the vector field (br,bth,bph), its dimension (nr,nlat,nlon), and
;      its indexing (rix,thix,phix,lat,lon).
;
;  KEYWORDS:
;    rixrange = a two-element vector containing radial indices (not values!)
;               that is used to restrict the range of radii within which null
;               points are searched for
;    nnulls = on output, contains the number of nulls located by the routine
;    interpolates = on output, returns interpolation indices of each null
;                   within the [phi,theta,r] index arrays
;    quiet = set to suppress all informational messages
;    debug = debug flag; set if debugging, increases completion time by a lot!
;
;  OUTPUTS:
;    result = a listing of [phi,theta,r] null points in radians
;
;  NOTES:
;    -The rixrange keyword can be used to, for example, filter out the forest
;     of nulls near the lower boundary, or filter out the line of nulls at the
;     upper boundary that occurs within PFSS models (that often cause problems
;     with this routine)
;    -There are many places which cause this routine to stop.  If you
;     encounter one of these, please tell me about them at derosa@lmsal.com!
;
;  MODIFICATION HISTORY:
;    M.DeRosa - 15 Nov 2010 - created
;               16 Feb 2011 - added quiet keyword
;               17 Aug 2011 - gave Newton-Raphson scheme a grid of starting
;                             points to start from when iterating to find a
;                             null
;               24 Feb 2016 - when the Newton-Raphson scheme fails, in many
;                             cases this was due to the null point lying very
;                             close to an edge or corner of the cube... if it
;                             fails with a "problems with Newton-Raphson"
;                             message, routine now tries again with a finer
;                             grid of starting points
;                3 Mar 2016 - now properly treats the periodicity in phi
;                             direction via the lonbounds tag in the input
;                             sph_data structure
;
;-
;---------------------------------------------------------------------------

function spherical_get_nulls_evalB,xvec,dmatrix=dmatrix
  
;  function for newton_raphson_3d.pro, does trilinear interpolation

;  common block, where corners8 = 8 corner points x 3 vector components
common spherical_get_nulls_evalB_block,corners8

;  idiot check
sizecorners8=size(corners8,/dim)
if ((sizecorners8(0) ne 8) or (sizecorners8(1) ne 3)) then begin
  print,'  ERROR in spherical_get_nulls_evalB: corners8 is not correct size'
  return,-1
endif
if n_elements(xvec) lt 3 then begin
  print,'  ERROR in spherical_get_nulls_evalB: xvec must have 3 elements'
endif

;  redefine field point
p0=xvec(0:2)

;  compute coefficients
avec=reform(corners8(0,*))
bvec=reform(corners8(1,*))-avec
cvec=reform(corners8(2,*))-avec
dvec=reform(corners8(3,*))-avec-bvec-cvec
evec=reform(corners8(4,*))-avec
fvec=reform(corners8(5,*))-avec-bvec-evec
gvec=reform(corners8(6,*))-avec-cvec-evec
hvec=reform(corners8(7,*))-avec-bvec-cvec-dvec-evec-fvec-gvec

;  compute derivative matrix
dmatrix=[[bvec+dvec*p0(1)+fvec*p0(2)+hvec*p0(1)*p0(2)],$
         [cvec+dvec*p0(0)+gvec*p0(2)+hvec*p0(0)*p0(2)],$
         [evec+fvec*p0(0)+gvec*p0(1)+hvec*p0(0)*p0(1)]]

;  compute value of function
return,[avec+bvec*p0(0)+cvec*p0(1)+dvec*p0(0)*p0(1)+evec*p0(2)+ $
        fvec*p0(0)*p0(2)+gvec*p0(1)*p0(2)+hvec*p0(0)*p0(1)*p0(2)]

end

;---------------------------------------------------------------------------

function spherical_get_nulls,sph_data,rixrange=rixrange,nnulls=nnulls,$
  interpolates=interpolates,quiet=quiet,debug=debug

;  usage message
if n_elements(sph_data) eq 0 then begin
  print,'  ERROR in spherical_get_nulls: no input data provided'
  print,'  Calling sequence: result=spherical_get_nulls(sph_data,rixrange=rixrange,nnulls=nnulls,interpolates=interpolates,/quiet,/debug)'
  return,-1
endif

;  common block for spherical_get_nulls_evalB routine above
common spherical_get_nulls_evalB_block,corners8

;  check to make sure field variables are defined
if ( (not ptr_valid(sph_data.br)) or (not ptr_valid(sph_data.bth)) or $
  (not ptr_valid(sph_data.bph)) ) then begin
  print,'  ERROR in spherical_get_nulls: br,bth, and/or bph not defined'
  return,-1
endif

;  take care of periodicity in phi
sfield_data=sph_data
if sph_data.lonbounds(0) lt 0 then begin
  sfield_data.bph=ptr_new([*sph_data.bph,(*sph_data.bph)(0,*,*)])
  sfield_data.bth=ptr_new([*sph_data.bth,(*sph_data.bth)(0,*,*)])
  sfield_data.br=ptr_new([*sph_data.br,(*sph_data.br)(0,*,*)])
  sfield_data.nlon++
  sfield_data.lon=ptr_new([*sph_data.lon,(*sph_data.lon)(0)+360])
  sfield_data.phi=ptr_new([*sph_data.phi,(*sph_data.phi)(0)+2*!dpi])
endif

;  setup
nax=size(*sfield_data.br,/dim)
nnulls=0
nulls=[0.,0.,0.]  ;  initialize null array
interpolates=[0.,0.,0.]  ;  initialize interpolates array

;  step 1: look at the voxels defined by the Br, Bth, and Bth grids, and see
;  whether, at the eight corners of each cell, any of these quantites change
;  sign (if for any variable the corner points are all of like sign, then
;  there cannot be a null inside)
signbr=sign_mld(*sfield_data.br)
signbth=sign_mld(*sfield_data.bth)
signbph=sign_mld(*sfield_data.bph)
whnull=where((signbr eq 0) and (signbth eq 0) and (signbph eq 0),nwhnull)
if nwhnull gt 0 then stop,'null detected on a gridpoint'
nullcandidates= $
  ( (signbr ne shift(signbr,[-1,0,0])) or $
    (signbr ne shift(signbr,[0,-1,0])) or $
    (signbr ne shift(signbr,[0,0,-1])) or $
    (signbr ne shift(signbr,[-1,-1,0])) or $
    (signbr ne shift(signbr,[-1,0,-1])) or $
    (signbr ne shift(signbr,[0,-1,-1])) or $
    (signbr ne shift(signbr,[-1,-1,-1])) ) and $
  ( (signbth ne shift(signbth,[-1,0,0])) or $
    (signbth ne shift(signbth,[0,-1,0])) or $
    (signbth ne shift(signbth,[0,0,-1])) or $
    (signbth ne shift(signbth,[-1,-1,0])) or $
    (signbth ne shift(signbth,[-1,0,-1])) or $
    (signbth ne shift(signbth,[0,-1,-1])) or $
    (signbth ne shift(signbth,[-1,-1,-1])) ) and $
  ( (signbph ne shift(signbph,[-1,0,0])) or $
    (signbph ne shift(signbph,[0,-1,0])) or $
    (signbph ne shift(signbph,[0,0,-1])) or $
    (signbph ne shift(signbph,[-1,-1,0])) or $
    (signbph ne shift(signbph,[-1,0,-1])) or $
    (signbph ne shift(signbph,[0,-1,-1])) or $
    (signbph ne shift(signbph,[-1,-1,-1])) )

;  zero out points on far edges 
nullcandidates(*,*,nax(2)-1)=0b
nullcandidates(*,nax(1)-1,*)=0b
nullcandidates(nax(0)-1,*,*)=0b  ;  wrapped arrays above deal with periodicity 
if keyword_set(rixrange) then begin
  rixrange(0)=(round(rixrange(0))>0)<(sfield_data.nr-2)
  rixrange(1)=(round(rixrange(1))>1)<(sfield_data.nr-1)
  if rixrange(0) ge 1 then nullcandidates(*,*,0:rixrange(0)-1)=0b
  if rixrange(1) le (sfield_data.nr-2) then nullcandidates(*,*,rixrange(1):*)=0b
endif

;  get list of candidate null points
whst1=where(nullcandidates,nwhst1)

;  if debugging, get indices of candidate null locations found in step 1
if keyword_set(debug) then begin
  if nwhst1 gt 0 then begin
    print,'  step 1: found '+strcompress(nwhst1,/r)+' candidate null cubes'
    step1ph=whst1 mod nax(0)
    step1th=whst1/nax(0)-(whst1/(nax(0)*nax(1)))*nax(1)
    step1r=whst1/(nax(0)*nax(1))
    window,0,xsiz=600,ysiz=600
    window,1,xsiz=1000,ysiz=750
  endif else begin
    if not keyword_set(quiet) then print,'  eliminated all cells after step 1'
  endelse
endif

;  step 2: for each candidate null cube (defined as in step 1 above),
;  determine on each face the contours for which Br=0, Bth=0, and Bph=0, and
;  then find out the locations where these contours cross each other

;  step 3: in general, because each of the intersection curves (Br=Bth=0,
;  Br=Bph=0, Bth=Bph=0) traverses the interior of the cube, creating pairs of
;  points in step 2, that means that all we have to do is follow these curves
;  thorughout the cube to find when the third variable is equal to zero

;  loop through each candidate zero point and look for nulls
grid=[0,1,nax(0),nax(0)+1,nax(0)*nax(1),nax(0)*nax(1)+1, $
  nax(0)*(nax(1)+1),nax(0)*(nax(1)+1)+1]
gridgrid=grid([[0,1,2,3],[4,5,6,7],[0,1,4,5],[2,3,6,7],[0,2,4,6],[1,3,5,7]])
corners=fltarr(4,6,3)  ;  4 corner points x 6 faces x 3 variables
facezero=bytarr(7,3)  ;  (6+1) faces x 3 variable pairs
facecoords=fltarr(2,7,3)  ;  2 coordinates x (6+1) faces x 3 variable pairs
facezvals=fltarr(7,3)  ;  (6+1) faces x 3 variable pairs
varpairs=[[1,2],[0,2],[0,1]]  ;  pairs ordered so that i=missing variable
if nwhst1 gt 0 then begin
  for i=0,nwhst1-1 do begin

    ;  progress report
    if (nwhst1 gt 10000) then if ~keyword_set(quiet) then $
      pfss_print_time,'  spherical_get_nulls: ',i+1,nwhst1,tst,slen
    if keyword_set(debug) then print

    ;  get corner points
    corners(*,*,0)=(*sfield_data.bph)(whst1(i)+gridgrid)
    corners(*,*,1)=(*sfield_data.bth)(whst1(i)+gridgrid)
    corners(*,*,2)=(*sfield_data.br)(whst1(i)+gridgrid)

    ;  set facearr (bit is set if B_i=0 contour exists on face j)
    facearr=abs(total(sign_mld(corners),1,/pres)) lt 4

    ;  determine whether B[ph,th,r]=0 lines cross on the various faces
    for j=0,2 do facezero(*,j)=$
      [facearr(*,(j+1) mod 3) and facearr(*,(j+2) mod 3),0b]

    if keyword_set(debug) then begin
      print
      print,'  steps 2 and 3: now doing cube '+strcompress(i,/r)
      print,'  there are '+strcompress(nwhst1,/r)+' cubes total'
      print,'  facearr array [6 faces, 3 variables] = '
      print,facearr
      print,'  facezero array [6+1 faces, 3 variable pairs] = '
      print,facezero
    endif

    ;  debug block, plots Br=Bth=Bph=0 contours on all six faces of voxel
    if keyword_set(debug) then begin
      wset,1
      erase
      plots,[0.26,0.02,0.02,0.98,0.98,0.26,0.26,0.5,0.5,0.26,0.26], $
            [0.34,0.34,0.66,0.66,0.34,0.34,0.98,0.98,0.02,0.02,0.34],/norm
      plots,[0.74,0.74],[0.34,0.66],/norm
      xyouts,0.74,0.80,'0',charsiz=2,/norm
      xyouts,0.78,0.80,'1',charsiz=2,/norm
      xyouts,0.74,0.77,'2',charsiz=2,/norm
      xyouts,0.74,0.83,'3',charsiz=2,/norm
      xyouts,0.72,0.80,'4',charsiz=2,/norm
      xyouts,0.76,0.80,'5',charsiz=2,/norm

      y=(x=linrange(100,0,1))
      xgrid=x#replicate(1,100)
      ygrid=replicate(1,100)#y
      for j=0,5 do begin  ;  loop through faces
        for k=0,2 do begin  ;  loop through variables
          a1=corners(0,j,k)
          b1=corners(1,j,k)-a1
          c1=corners(2,j,k)-a1
          d1=corners(3,j,k)-a1-b1-c1
          case j of
            0: contour,a1+b1*xgrid+c1*ygrid+d1*xgrid*ygrid,x,y, $
                      lev=[0],c_line=[k],/noer,pos=[0.26,0.34,0.5,0.66], $
                      /norm,xsty=5,ysty=5
            1: contour,a1+b1*xgrid+c1*ygrid+d1*xgrid*ygrid,reverse(x),y, $
                      lev=[0],c_line=[k],/noer,pos=[0.74,0.34,0.98,0.66], $
                      /norm,xsty=5,ysty=5
            2: contour,a1+b1*xgrid+c1*ygrid+d1*xgrid*ygrid,x,reverse(y), $
                      lev=[0],c_line=[k],/noer,pos=[0.26,0.02,0.5,0.34], $
                      /norm,xsty=5,ysty=5
            3: contour,a1+b1*xgrid+c1*ygrid+d1*xgrid*ygrid,x,y, $
                      lev=[0],c_line=[k],/noer,pos=[0.26,0.66,0.5,0.98], $
                      /norm,xsty=5,ysty=5
            4: contour,transpose(a1+b1*xgrid+c1*ygrid+d1*xgrid*ygrid), $
                      reverse(x),y, $
                      lev=[0],c_line=[k],/noer,pos=[0.02,0.34,0.26,0.66], $
                      /norm,xsty=5,ysty=5
            5: contour,transpose(a1+b1*xgrid+c1*ygrid+d1*xgrid*ygrid),x,y, $
                      lev=[0],c_line=[k],/noer,pos=[0.5,0.34,0.74,0.66], $
                      /norm,xsty=5,ysty=5
            else: stop,'j out of range!'  ;  shouldn't be able to get here
          endcase
        endfor
      endfor

    endif

    ;  step 2: determine points on faces where Bph=Bth=0, Bph=Br=0, Bth=Br=0 
    for j=0,5 do begin ;  loop through faces
      for k=0,2 do begin  ;  loop through variable pairs
        if facezero(j,k) then begin  ;  see if there is a crossing

          ;  compute [a,b,c,d] in Bx(x,y) = a + bx + cy + dxy
          a1=corners(0,j,varpairs(0,k))
          b1=corners(1,j,varpairs(0,k))-a1
          c1=corners(2,j,varpairs(0,k))-a1
          d1=corners(3,j,varpairs(0,k))-a1-b1-c1
          a2=corners(0,j,varpairs(1,k))
          b2=corners(1,j,varpairs(1,k))-a2
          c2=corners(2,j,varpairs(1,k))-a2
          d2=corners(3,j,varpairs(1,k))-a2-b2-c2

          ;  debug block
          if keyword_set(debug) then begin
            y=(x=linrange(100,-1,2))
            xgrid=x#replicate(1,100)
            ygrid=replicate(1,100)#y
            wset,0
            contour,a1+b1*xgrid+c1*ygrid+d1*xgrid*ygrid,x,y,lev=[0]
            contour,a2+b2*xgrid+c2*ygrid+d2*xgrid*ygrid,x,y,lev=[0],/noer
            plots,[0,0,1,1,0],[0,1,1,0,0]
          endif
 
          ;  solve quadratic equations for x
          quada1=b1*d2-b2*d1
          quadb1=a1*d2-a2*d1+b1*c2-b2*c1
          quadc1=a1*c2-a2*c1
          disc1=quadb1*quadb1-4*quada1*quadc1

          ;  find root on face, if one exists
          gotzero=0
          if disc1 ge 0 then begin  ;  check if discriminant is positive

            ;  real roots exist, compute them
            xroots=(-quadb1+[-1,1]*sqrt(disc1))/(2*quada1)
            yfromx1=-(a1+b1*xroots)/(c1+d1*xroots)
            ;yfromx2=-(a2+b2*xroots)/(c2+d2*xroots)  ;  should equal yfromx1

            ;  determine whether the (x,y) points are on the face
            if (xroots(0) ge 0) and (xroots(0) le 1) and $
              (yfromx1(0) ge 0) and (yfromx1(0) le 1) then begin
              facecoords(*,j,k)=[xroots(0),yfromx1(0)]
              gotzero=1
            endif
            if (xroots(1) ge 0) and (xroots(1) le 1) and $
              (yfromx1(1) ge 0) and (yfromx1(1) le 1) then begin
              if gotzero eq 1 then begin  ;  double root on same face
                facezero(6,k)=1b
                facecoords(*,6,k)=[xroots(1),yfromx1(1)]
                gotzero=2
              endif else begin
                facecoords(*,j,k)=[xroots(1),yfromx1(1)]
                gotzero=1
              endelse
            endif
            if gotzero eq 0 then facezero(j,k)=0b  ;  no zeroes found on face

          endif else facezero(j,k)=0b  ;  no real roots

          ;  debugging stuff
          if keyword_set(debug) then begin

            if (facezero(j,k) gt 0) then begin
              print,'  cube '+strcompress(i,/r)+' face '+strcompress(j,/r)+$
                ' variable pair '+strcompress(k,/r)+': intersection found'
            endif else begin
              print,'  cube '+strcompress(i,/r)+' face '+strcompress(j,/r)+$
                ' variable pair '+strcompress(k,/r)+': no intersection found'
            endelse
            if gotzero eq 2 then print,'  note: multiple nulls found on face'

            ;  solve quadratic equation for y instead of x
            quada2=c1*d2-c2*d1
            quadb2=a1*d2-a2*d1-b1*c2+b2*c1
            quadc2=a1*b2-a2*b1
            disc2=quadb2*quadb2-4*quada2*quadc2
            if disc2 ge 0 then begin
              yroots=(-quadb2+[-1,1]*sqrt(disc2))/(2*quada2)  
              xfromy1=-(a1+c1*yroots)/(b1+d1*yroots)  ;  \  both should be equal
              xfromy2=-(a2+c2*yroots)/(b2+d2*yroots)  ;  /  to xroots variable
            endif else print,'  no real roots'
          endif

        endif
      endfor
    endfor

    ;  step 3: examine intersection curves within cube where Bph=Bth=0,
    ;  Bph=Br=0, Bth=Br=0 to determine whether third compoment is ever 0
    facepairs=total(facezero,1,/pres)
    for j=0,2 do begin  ;  loop through variable pairs
      if facepairs(j) gt 0 then begin
        if facepairs(j) eq 2 then begin

          ;  get value of the third coordinate at intersection points on faces
          whfaces=where(facezero(*,j))
          for k=0,1 do begin
            if whfaces(k) lt 6 then begin
              ;  if whfaces(k)=6, then use a1,b1,c1,d1 from before
              a1=corners(0,whfaces(k),j)  ;  j indexes "other" coordinate
              b1=corners(1,whfaces(k),j)-a1
              c1=corners(2,whfaces(k),j)-a1
              d1=corners(3,whfaces(k),j)-a1-b1-c1
            endif
            facezvals(whfaces(k),j)=$
              a1+b1*facecoords(0,whfaces(k),j)+c1*facecoords(1,whfaces(k),j)+$
              d1*facecoords(0,whfaces(k),j)*facecoords(1,whfaces(k),j)

            ;  debugging stuff
            if keyword_set(debug) then begin
              signzval=sign_mld(facezvals(whfaces(k),j))
              print,'  cube '+strcompress(i,/r)+' variable pair '+$
                strcompress(j,/r)+': found '+(['-','zero','+'])(signzval+1)+$
                ' z-value at intersection curve'
              if whfaces(k) lt 6 then begin
                ;  if whfaces(k)=6, then use a2,b2, etc., from before
                a2=corners(0,whfaces(k),j)
                b2=corners(1,whfaces(k),j)-a2
                c2=corners(2,whfaces(k),j)-a2
                d2=corners(3,whfaces(k),j)-a2-b2-c2
                a1a=corners(0,whfaces(k),varpairs(0,j))
                b1a=corners(1,whfaces(k),varpairs(0,j))-a1a
                c1a=corners(2,whfaces(k),varpairs(0,j))-a1a
                d1a=corners(3,whfaces(k),varpairs(0,j))-a1a-b1a-c1a
                a2a=corners(0,whfaces(k),varpairs(1,j))
                b2a=corners(1,whfaces(k),varpairs(1,j))-a2a
                c2a=corners(2,whfaces(k),varpairs(1,j))-a2a
                d2a=corners(3,whfaces(k),varpairs(1,j))-a2a-b2a-c2a
              endif
              zvals2=$  ;  should be the same as what was put into facezvals
                a2+b2*facecoords(0,whfaces(k),j)+c2*facecoords(1,whfaces(k),j)+$
                d2*facecoords(0,whfaces(k),j)*facecoords(1,whfaces(k),j)
              contour,a1a+b1a*xgrid+c1a*ygrid+d1a*xgrid*ygrid,x,y,lev=[0]
              contour,a2a+b2a*xgrid+c2a*ygrid+d2a*xgrid*ygrid,x,y,lev=[0],/noer
              contour,a2+b2*xgrid+c2*ygrid+d2*xgrid*ygrid,x,y,lev=[0],/noer,/do
              plots,[0,0,1,1,0],[0,1,1,0,0]
            endif

          endfor

          ;  if null exists (i.e., if value of third coordinate on
          ;  intersection curves are of opposite sign), then find the null!
          if product(facezvals(whfaces,j)) lt 0 then begin

            ;  use Newton-Raphson routine to determine null
            gran=3l  ;  granularity factor to sample cube for starting points
            corners8=reform(corners(*,0:1,*),[8,3])  ;  for common block
            garr=(linrange(gran+2,0,1))(1:1+gran-1)
            gxgrid=rebin(reform(garr#replicate(1,gran),[gran,gran,1]), $
                         [gran,gran,gran])
            gygrid=transpose(gxgrid,[1,2,0])
            gzgrid=transpose(gxgrid,[2,1,0])
            p0arr=fltarr(3,gran^3)
            invstatus=intarr(gran^3)
            incube=bytarr(gran^3)
            for k=0,gran^3-1 do begin
              p0=newton_raphson_3d('spherical_get_nulls_evalB',$
                [gxgrid(k),gygrid(k),gzgrid(k)],status=invstat)
              p0arr(*,k)=p0
              invstatus(k)=invstat
              incube(k)=(total((p0 ge 0) and (p0 le 1),/pres) eq 3)
            endfor
            if total(invstatus gt 0) eq gran^3 then begin  ;  incr granularity
              gran=10l  ;  granularity factor to sample cube for starting pts
              garr=(linrange(gran+2,0,1))(1:1+gran-1)
              gxgrid=rebin(reform(garr#replicate(1,gran),[gran,gran,1]), $
                           [gran,gran,gran])
              gygrid=transpose(gxgrid,[1,2,0])
              gzgrid=transpose(gxgrid,[2,1,0])
              p0arr=fltarr(3,gran^3)
              invstatus=intarr(gran^3)
              incube=bytarr(gran^3)
              for k=0,gran^3-1 do begin
                p0=newton_raphson_3d('spherical_get_nulls_evalB',$
                  [gxgrid(k),gygrid(k),gzgrid(k)],status=invstat)
                p0arr(*,k)=p0
                invstatus(k)=invstat
                incube(k)=(total((p0 ge 0) and (p0 le 1),/pres) eq 3)
              endfor
              if total(invstatus gt 0) eq gran^3 then begin
                gran=30l  ;  granularity factor to sample cube for starting pts
                garr=(linrange(gran+2,0,1))(1:1+gran-1)
                gxgrid=rebin(reform(garr#replicate(1,gran),[gran,gran,1]), $
                             [gran,gran,gran])
                gygrid=transpose(gxgrid,[1,2,0])
                gzgrid=transpose(gxgrid,[2,1,0])
                p0arr=fltarr(3,gran^3)
                invstatus=intarr(gran^3)
                incube=bytarr(gran^3)
                for k=0,gran^3-1 do begin
                  p0=newton_raphson_3d('spherical_get_nulls_evalB',$
                     [gxgrid(k),gygrid(k),gzgrid(k)],status=invstat)
                  p0arr(*,k)=p0
                  invstatus(k)=invstat
                  incube(k)=(total((p0 ge 0) and (p0 le 1),/pres) eq 3)
                endfor
                if total(invstatus gt 0) eq gran^3 then $
                  stop,'problems with Newton-Raphson' ; incr granularity again?
              endif
              
            endif

            ;  determine value of B at minima
            p0sarr=[(whst1(i) mod nax(0))+p0arr(0,*), $
              (whst1(i)/nax(0)-(whst1(i)/(nax(0)*nax(1)))*nax(1))+p0arr(1,*),$
              (whst1(i)/(nax(0)*nax(1)))+p0arr(2,*)]
            sphp0arr=[interpolate(*sfield_data.lon,p0sarr(0,*)), $
              interpolate(*sfield_data.lat,p0sarr(1,*)), $
              interpolate(*sfield_data.rix,p0sarr(2,*))]
            bmag=sqrt(total([ $
              interpolate(*sfield_data.bph,p0sarr(0,*),p0sarr(1,*),p0sarr(2,*)),$
              interpolate(*sfield_data.bth,p0sarr(0,*),p0sarr(1,*),p0sarr(2,*)),$
              interpolate(*sfield_data.br ,p0sarr(0,*),p0sarr(1,*),p0sarr(2,*))] $
              ^2,1))

            ;  set p0, p0s, and sphp0 for "better" null
            whincube=where(incube and (invstatus eq 0),nwhincube)
            if nwhincube gt 0 then begin
              buff=min(bmag(whincube),whmin)
              p0=p0arr(*,whincube(whmin))
              p0s=p0sarr(*,whincube(whmin))
              sphp0=sphp0arr(*,whincube(whmin))
            endif

            ;  see if the current null is the same as those prev cataloged
            if nwhincube gt 0 then begin
              if nnulls gt 0 then begin
                found=intarr(nnulls+1)
                for k=1,nnulls do begin
                  dist=sqrt((p0s(0)-interpolates(0,k))^2+$
                       (p0s(1)-interpolates(1,k))^2+$
                       (p0s(2)-interpolates(2,k))^2)
                  if (dist lt 0.1) then found(k)=1
                endfor
                if total(found,/pres) eq 0 then begin
                  nnulls=nnulls+1
                  nulls=[[nulls],[sphp0]]
                  interpolates=[[interpolates],[p0s]]
                  if keyword_set(debug) then print,'  new null found'
                endif else begin
                  if keyword_set(debug) then print,'  null already flagged'
                endelse
              endif else begin
                nnulls=nnulls+1
                nulls=[[nulls],[sphp0]]
                interpolates=[[interpolates],[p0s]]
                if keyword_set(debug) then print,'  new null found'
              endelse
            endif else begin
              if keyword_set(debug) then print,'  null is outside cube'
            endelse

          endif else begin
            if keyword_set(debug) then begin
              print,'  cube '+strcompress(i,/r)+' variable pair '+$
                strcompress(j,/r)+': pair of z-values have same sign, skipping'
            endif
          endelse
          
        endif else begin  ;  look for null inside cube anyway

          ;  use Newton-Raphson routine to determine null
          corners8=reform(corners(*,0:1,*),[8,3])  ;  for common block
          p0=newton_raphson_3d('spherical_get_nulls_evalB',[0.5,0.5,0.5], $
            status=invstatus,itmax=50,loud=keyword_set(debug))
          if invstatus gt 0 then flag=1 else flag=0

          ;  keep null only if it is within cube, and no problems encountered
          if (total((p0 ge 0) and (p0 le 1),/pres) eq 3) and (flag eq 0) $
            then begin

            ;  convert form normalized coords to (phi,theta,r)
            p0s=[(whst1(i) mod nax(0))+p0(0), $
                 (whst1(i)/nax(0)-(whst1(i)/(nax(0)*nax(1)))*nax(1))+p0(1),$
                 (whst1(i)/(nax(0)*nax(1)))+p0(2)]
            sphp0=[interpolate(*sfield_data.lon,p0s(0)), $
                   interpolate(*sfield_data.lat,p0s(1)), $
                   interpolate(*sfield_data.rix,p0s(2))]

            ;  see if the current null is the same as those prev cataloged
            if nnulls gt 0 then begin
              found=intarr(nnulls+1)
              for k=1,nnulls do begin
                dist=sqrt((p0s(0)-interpolates(0,k))^2+$
                     (p0s(1)-interpolates(1,k))^2+$
                     (p0s(2)-interpolates(2,k))^2)
                if (dist lt 0.1) then found(k)=1
              endfor
              if total(found,/pres) eq 0 then begin
                nnulls=nnulls+1
                nulls=[[nulls],[sphp0]]
                interpolates=[[interpolates],[p0s]]
                if keyword_set(debug) then print,'  new null found'
              endif else begin
                if keyword_set(debug) then print,'  null already flagged'
              endelse
            endif else begin
              nnulls=nnulls+1
              nulls=[[nulls],[sphp0]]
              interpolates=[[interpolates],[p0s]]
              if keyword_set(debug) then print,'  new null found'
            endelse

          endif else begin
            if keyword_set(debug) then print,'  null is outside cube'
          endelse

        endelse

      endif
    endfor

  endfor
endif

;  debug block
if keyword_set(debug) then begin
  if nnulls gt 0 then begin
    print,'  null list: [nullno,r,lat,lon,br,bth,bph]'
    for i=1,nnulls do begin
      iphc=interpolates(0,i)
      ithc=interpolates(1,i)
      irc=interpolates(2,i)
      bval=[interpolate(*sfield_data.bph,iphc,ithc,irc), $
            interpolate(*sfield_data.bth,iphc,ithc,irc), $
            interpolate(*sfield_data.br,iphc,ithc,irc)]
      print,i,[nulls(*,i),bval]
    endfor
  endif else print,'no nulls found'
endif

;  set up return variables
if not keyword_set(quiet) then $
  print,'  spherical_get_nulls: found '+strcompress(nnulls,/r)+' null points'
if nnulls gt 0 then begin
  nulls(0,*)=nulls(0,*)*!dpi/180
  nulls(1,*)=!dpi/2-nulls(1,*)*!dpi/180
  if nnulls gt 1 then interpolates=reverse(interpolates(*,1:*),2) $
    else interpolates=interpolates(*,1:*)
  if nnulls gt 1 then return,reverse(nulls(*,1:*),2)  $
    else return,nulls(*,1:*)  ;  order nulls from highest to lowest
endif else return,-1

stop,'  shouldn''t be able to get here'

end
