;+
;  NAME: spherical_image_data__define
;
;  PURPOSE:  
;    Creates a structure of type spherical_image_data, containing fields for
;    spherical image data (i.e. data mapped on a spherical surface) and its
;    associated metadata.
;
;  INPUTS: none
;
;  OUTPUTS:
;    Defines a structure with the following fields:
;      *image: two-dimensional data, indexed (lon,lat), typically binary
;      rad: the radius of the spherical surface
;      nlat: number of gridpoints in latitudinal (theta) dimension
;      nlon: number of gridpoints in longitudinal (phi) dimension
;      *lat: index array for meridional dimension, in degrees
;            between [0,180]
;      *lon: index array for zonal dimension, in degrees
;            between [0,360]
;      *theta: index array for theta dimension, should be set to 
;              (90-lat)*!dpi/180.
;      *phi: index array for phi dimension, should be set to lon*!dpi/180.
;      palette: instance of the IDLgrPalette object class corresponding to the
;               RGB values of the color lookup table associated with the image
; 
;  CALLING SEQUENCE:
;    data={spherical_image_data}
; 
;  NOTES:
;    1.  Any field preceded by a * indicates that this field is a pointer.
;    2.  The data needs to be on a rectangular (i.e., longitude-latitude)
;        grid, but the grid spacing does not need to be uniform as long as
;        both the lon,lat fields each increase monotonically.
; 
;  MODIFICATION HISTORY:
;    M.DeRosa - 14 Dec 2005 - created
;               13 Jul 2007 - added palette tag to structure definition
;
;-

pro spherical_image_data__define

;  dummy structure
dummy={spherical_image_data, $
  image:ptr_new(),rad:1d,nlat:-1l,nlon:-1l,theta:ptr_new(),phi:ptr_new(),$
    lat:ptr_new(),lon:ptr_new(),palette:obj_new()}

end
