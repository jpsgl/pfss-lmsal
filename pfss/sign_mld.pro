;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
;
;  sign_mld.pro - this program takes an input array and returns an integer
;                 array of the same shape where every element is either -1
;                 0 or 1, signifying whether the corresponding element in
;                 the input array is negative, zero, or positive.
;
;  usage:  result=sign(array)
;       where:  array = input array on which to perform the signing
;               result = output array which contains the sign
;
;  notes: 
;
;  M.DeRosa - 24 Jun 1999 - created
;             15 May 2002 - changed name to sign_mld so as not to confuse
;                           with SSW version by the same name
;
;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

function sign_mld,array

;  usage message
if n_params() eq 0 then begin
  print,'  result=sign(array)'
  return,0
endif

pos=array gt 0
neg=array lt 0
return,-1*neg+pos

end
