;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
;
;  get_string_number.pro - This function creates a string out of a number
;
;  usage:  out = get_string_number(number,pad=pad)
;             where out = output string
;                   number = input number
;                   pad = number of characters the string must have, routine
;                         pads with leading zeroes
;
;  M.DeRosa -  1 Aug 2000 - created
;             14 Nov 2002 - added capability to process an array of numbers
;             29 Jan 2003 - now returns a scalar (instead of a one-element
;                           vector) if number is a scalar
;
;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

function get_string_number,number,pad=pad

nel=n_elements(number)
strout=strarr(nel)
for i=0,nel-1 do begin
  st=strcompress(round(number(i)),/r)
  if keyword_set(pad) then while strlen(st) lt pad(0) do st='0'+st
  strout(i)=st
endfor

if nel eq 1 then return,strout(0) else return,strout

end
