;+
;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
;
;  pfss_trace_field - This procedure traces the vector field through a
;                     spherical shell or segment volume, given an array of
;                     starting points
;
;  usage:  pfss_trace_field,kind,stbr,stbth,stbph,outfield=outfield,
;              stepmax=stepmax,safety=safety,
;              /endpoints,/oneway,/noreverse,/old,/quiet
;          where kind = on output, contains kind of fieldline:
;                      -1=line starting point out of bounds
;                       0=error of some sort?
;                       1=maximum step limit reached
;                       2=line intersects inner and outer boundaries, 
;                       3=both endpoints of line lie on inner boundary, 
;                       4=both endpoints of line lie on outer boundary,
;                       5=line intersects inner and side boundaries,
;                       6=line intersects outer and side boundaries,
;                       7=both endpoints of line lie on side boundary/ies
;                       8=one of the endpoints is a null
;                (stbr,stbth,stbph) = on output, (r,theta,phi) components 
;                                     of B at starting point
;                outfield = an array of dimension [nlines,3,3], containing the
;                           three components of the vector field at both
;                           endpoints and the starting point for each line.
;                           The first dimension indexes the fieldlines, the
;                           second dimension indexes the points of interest
;                           (endpoint #1, starting point, endpoint #2), and
;                           the third dimension indexes the coordinate
;                           (r,theta,phi).
;                stepmax = max number of steps per field line (default=3000)
;                safety = maximum ds along each field line, in units of 
;                         minimum grid spacing (default = 0.5)
;                endpoints = set if endpoints only are desired
;                oneway = set if loops are to be traced in one direction only,
;                         either upward or downward depending on whether
;                         starting point is closer to the bottom or to the top
;                         of the domain, and then if the starting point is no 
;                         more than 1% away from the upper or lower boundary
;                noreverse = set if field lines are not to be reversed (by
;                         default, closed field lines are oriented so that the
;                         end with negative polarity comes first and open 
;                         field lines are oriented so that the photospheric
;                         end comes first
;                old = uses old (original version of this routine) if set
;                quiet = set if minimal screen output is desired
;
;          and in the common block we have:
;                (br,bth,bph) = on input, (r,theta,phi) components of field
;                (str,stth,stph) = on input, contains an n-vector (where 
;                                  n=number of field lines) of starting
;                                  coordinates for each field line
;                (ptr,ptth,ptph) = on output, contains a (n,stepmax)-array of
;                                  field line coordinates
;                nstep = on output, an n-vector containing the number of 
;                        points comprising each field line
;
;  To do: -How does one tell if line has closed on itself?  Not that it's
;          mathematically allowed in a potential field setting...
;
;  M.DeRosa -  8 Feb 2002 - converted from an earlier script
;             27 Jun 2002 - added quiet keyword
;              7 Oct 2002 - accommodates tracing in both directions, 
;                           added kind keyword
;             29 Oct 2002 - added endpoints keyword
;             30 Oct 2002 - added stbr parameter
;             21 Nov 2002 - added stbth,stbph parameters
;              9 Dec 2002 - changed order of open field line indices so that
;                           the bottom end comes first
;              3 Jun 2003 - changed order of closed field line indices so that
;                           negative radial polarity comes first
;             25 Aug 2003 - fixed bug that caused incomplete field lines, due
;                           to the fact top wasn't set correctly as each line
;                           is traced
;             21 Jul 2004 - added oneway flag
;              2 Aug 2004 - added noreverse flag
;              5 Feb 2007 - added simple error check
;             16 Dec 2009 - routine is now merely a wrapper for the identical
;                           spherical_field_start_coord routine
;             21 Apr 2014 - added OUTFIELD as an explicit keyword (since it is
;                           specified in the call to spherical_trace_field, it
;                           does not properly return this variable to the user
;                           through EXKEY structure)
;
;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
;-

pro pfss_trace_field,kind,stbr,stbth,stbph,outfield=outfield,_ref_extra=exkey

;  convert to spherical_field_data structure
pfss_to_spherical,pfss_data,/free,/no_copy

;  call sister routine
spherical_trace_field,linekind=kind,outfield=outfield,pfss_data,_extra=exkey

;  deal with stbr,stbth,stbph outputs
stbr=outfield(*,1,0)
stbth=outfield(*,1,1)
stbph=outfield(*,1,2)

;  convert back to pfss common block
spherical_to_pfss,pfss_data,/noreset,/no_copy

;  OLD STUFF IS BELOW - commented out on 12/17/2009

;; pro pfss_trace_field,kind,stbr,stbth,stbph,stepmax=stepmax,safety=safety,endpoints=endpoints,oneway=oneway,noreverse=noreverse,quiet=quiet

;; ;  include common block
;; @pfss_data_block

;; ;  before doing anything, first set keywords in the _extra structure
;; if n_tags(exkey) gt 0 then begin
;;   keyallowed=['STEPMAX','SAFETY','ENDPOINTS','ONEWAY','OUTFIELD','LINEKIND', $
;;               'LINELENGTHS','NOREVERSE','TRIM','QUIET']
;;   kwords=tag_names(exkey)
;;   kwfound=bytarr(n_elements(kwords))
;;   for i=0,n_elements(kwords)-1 do begin
;;     wh=where(strpos(keyallowed,kwords(i)) eq 0,nwh)
;;     case nwh of
;;       0:  ;  do nothing (keyword not set)
;;       1:  err=execute(keyallowed(wh(0))+'='+strcompress(exkey.(i),/r))
;;       else: begin  ;  generate error since abbreviated keyword is ambiguous
;;         print,'pfss_trace_field: ERROR - ambiguous keyword: '+kwords(i)
;;         end
;;     endcase
;;   endfor
;; endif

;; ;  parameters
;; if n_elements(stepmax) eq 0 then stepmax=3000l else stepmax=long(stepmax(0))
;; if n_elements(safety) eq 0 then safety=0.5 else safety=float(safety(0))
;; npt=n_elements(str)  ;  npt = number of field lines to be drawn
;; rmin=min(rix,max=rmax)

;; ;  simple error check
;; if n_elements(str) eq 0 then begin
;;   print,'  pfss_trace_field: ERROR - variable str not set properly'
;;   return
;; endif

;; ;  get deltas
;; deltar=rix(1)-rix(0)
;; deltath=theta(0)-theta(1)
;; deltaph=(lon(1)-lon(0))*!dtor

;; ;  initialize arrays
;; nstep=lonarr(npt)
;; kind=intarr(npt)
;; stbph=(stbth=(stbr=fltarr(npt)))
;; if keyword_set(endpoints) then begin
;;   ptr=fltarr(2,npt)
;;   ptth=fltarr(2,npt)
;;   ptph=fltarr(2,npt)
;; endif else begin
;;   ptr=fltarr(stepmax,npt)
;;   ptth=fltarr(stepmax,npt)
;;   ptph=fltarr(stepmax,npt)
;; endelse
;; ptbr=fltarr(npt,2)
;; ptbth=fltarr(npt,2)
;; ptbph=fltarr(npt,2)
;; ptr(0,*)=str
;; ptth(0,*)=stth
;; ptph(0,*)=stph
;; ir=fltarr(stepmax)
;; ith=fltarr(stepmax)
;; iph=fltarr(stepmax)

;; ;  loop through each line
;; if not keyword_set(quiet) then begin
;;   print,'  pfss_trace_field: tracing '+strcompress(npt,/r)+' field lines ...'
;; endif
;; for i=0l,npt-1 do begin

;;   ;  print time left
;;   if not keyword_set(quiet) then $
;;     pfss_print_time,'  pfss_trace_field: ',i+1,npt,tst,slen1 $
;;     else tst=long(systime(1))

;;   ;  initialize
;;   ir(0)=ptr(0,i)
;;   ith(0)=ptth(0,i)
;;   iph(0)=ptph(0,i)
;;   step=1l

;;   ;  trace out line
;;   repeat begin

;;     ;  current point
;;     ptc=[ir(step-1),ith(step-1),iph(step-1)]  ;  (r,th,ph) or current point
;;     spth=sin(ptc(1))

;;     ;  calculate value of Br,Bth,Bph at current point
;;     irc=get_interpolation_index(rix,ptc(0))
;;     ithc=get_interpolation_index(lat,90-ptc(1)*!radeg)
;;     iphc=get_interpolation_index(lon,(ptc(2)*!radeg+360) mod 360)
;;     brc=interpolate(br,iphc,ithc,irc)
;;     bthc=interpolate(bth,iphc,ithc,irc)/ptc(0)
;;     bphc=interpolate(bph,iphc,ithc,irc)/(ptc(0)*spth)
;;     ds=reform([brc,bthc,bphc])

;;     ;  initialization
;;     if step eq 1 then begin

;;       ;  set initial fields
;;       ptbr(i,0)=brc
;;       ptbth(i,0)=bthc
;;       ptbph(i,0)=bphc

;;       ;  determine of loop is to be traced both ways or one way only
;;       if keyword_set(oneway) then begin

;;         ;  determine if starting point is at top, bottom or middle
;;         pct=(ptc(0)-rmin)/(rmax-rmin)
;;         case 1 of
;;           pct gt 0.99: begin
;;             top=1
;;             kind(i)=4
;;             end
;;           ((pct lt 0.01) and (float(rix(1)) ge 1.01)): begin
;;             top=-1
;;             kind(i)=3
;;             end
;;           ((ptc(0) lt float(rix(1))) or (pct lt 0.01)): begin
;;             top=-1
;;             kind(i)=3
;;             end
;;           else: top=0
;;         endcase

;;       endif else top=0

;;       ;  set initial stepsize and direction for first step
;;       stbr(i)=brc  &  stbth(i)=bthc  &  stbph(i)=bphc
;;       bsign=sign_mld(brc)
;;       steplen=safety*deltar/(abs(brc)>1.0)
;;       if top ge 0 then bsign=-bsign

;;     endif
;;     if bsign lt 0 then ds=-ds

;;     ;  take a step, repeat if too big
;;     stepflag=0
;;     repeat begin

;;       ;  step forward by an amount steplen along direction ds
;;       result=forw_euler(step-1,ptc,steplen,ds)

;;       ;  evaluate if current step was too big or too small
;;       diff=result-ptc
;;       err=max(abs(diff/[deltar,deltath,deltaph]))/safety
;;       case 1 of
;;         err gt 1: begin  ;  stepsize too big, reduce and do over
;;           steplen=0.5*steplen
;;           stepflag=0
;;           end
;;         err lt 0.1: begin  ;  stepsize too small, increase for next time
;;           steplen=2*steplen
;;           stepflag=1
;;           end
;;         else: stepflag=1  ;  don't change stepsize, just set flag to exit
;;       endcase
        
;;     endrep until stepflag

;;     ;  store in ir,ith,iph arrays
;;     ir(step)=result(0)
;;     ith(step)=result(1)
;;     iph(step)=result(2)

;;     ;  set flags
;;     hitsides=(ith(step) lt min(theta)) or (ith(step) gt max(theta))
;;     hittop=(ir(step) gt rmax)
;;     hitbot=(ir(step) lt rmin)
;;     hitstepmax=((step+1) eq stepmax)

;;     ;  deal with flags
;;     if (hitsides or hittop or hitbot or hitstepmax) then begin
;;       if top eq 0 then begin
;;         if hitstepmax then begin  ;  line may close on itself?
;;           kind(i)=1
;;           flag=1
;;         endif else begin  ;  hit boundary

;;           ;  reset flag
;;           flag=0

;;           ;  set case to indicate which boundary
;;           case 1 of
;;             hittop: begin
;;               kind(i)=4
;;               top=1
;;               end
;;             hitbot: begin
;;               kind(i)=3
;;               top=-1
;;               end
;;             hitsides: if kind(i) eq 7 then flag=1 else kind(i)=7
;;             else: begin
;;               print,'  shouldn''t be able to get here'
;;               stop
;;               end
;;           endcase

;;           ;  reverse order of data and continue in other direction
;;           ir(0:step)=reverse(ir(0:step))
;;           ith(0:step)=reverse(ith(0:step))
;;           iph(0:step)=reverse(iph(0:step))
          
;;           ;  reset initial fields
;;           ptbr(i,0)=brc
;;           ptbth(i,0)=bthc
;;           ptbph(i,0)=bphc

;;           ;  reset sign, step length
;;           bsign=-bsign

;;         endelse
;;       endif else begin
;;         flag=1
;;         kindc=kind(i)
;;         case 1 of
;;           hittop: begin
;;             case kindc of
;;               3: kind(i)=2  ;  I'm not sure this can happen
;;               4: kind(i)=4
;;               7: kind(i)=6
;;               else: begin
;;                 print,'  shouldn''t be able to get here'
;;                 stop
;;                 end
;;             endcase
;;             end
;;           hitbot: begin
;;             case kindc of
;;               3: kind(i)=3  ;  I'm not sure this can happen either 
;;               4: begin
;;                 kind(i)=2
;;                 if not keyword_set(noreverse) then begin
;;                   ir(0:step)=reverse(ir(0:step))
;;                   ith(0:step)=reverse(ith(0:step))
;;                   iph(0:step)=reverse(iph(0:step))
;;                 endif
;;                 end
;;               7: kind(i)=5
;;               else: begin
;;                 print,'  shouldn''t be able to get here'
;;                 stop
;;                 end
;;             endcase
;;             end
;;           hitsides: begin
;;             if ((kindc eq 3) or (kindc eq 4)) then begin
;;               kind(i)=kindc+2 
;;             endif else begin
;;               print,'  shouldn''t be able to get here'
;;               stop
;;             endelse
;;             end
;;           hitstepmax: kind(i)=1
;;         endcase
;;       endelse
;;     endif else flag=0

;;     ;  increment counter
;;     step=step+1

;;   endrep until flag
;;   nstep(i)=step

;;   ;  set final fields
;;   ptbr(i,1)=brc
;;   ptbth(i,1)=bthc
;;   ptbph(i,1)=bphc

;;   ;  if closed, make field line go from negative to positive
;;   if not keyword_set(noreverse) then begin
;;     if (kind(i) eq 3) and (ptbr(i,0) gt 0) then begin
;;       ir(0:step-1)=reverse(ir(0:step-1))
;;       ith(0:step-1)=reverse(ith(0:step-1))
;;       iph(0:step-1)=reverse(iph(0:step-1))
;;       ptbr(i,*)=reverse(ptbr(i,*))
;;       ptbth(i,*)=reverse(ptbth(i,*))
;;       ptbph(i,*)=reverse(ptbph(i,*))
;;     endif
;;   endif

;;   ;  fill pt arrays
;;   if keyword_set(endpoints) then begin
;;     ptr(*,i)=ir([0,step-1])
;;     ptth(*,i)=ith([0,step-1])
;;     ptph(*,i)=iph([0,step-1])
;;   endif else begin
;;     ptr(*,i)=ir
;;     ptth(*,i)=ith
;;     ptph(*,i)=iph
;;   endelse

;; endfor
;; ttime=round((long(systime(1))-tst)/60.)
;; if not keyword_set(quiet) then $
;;   print,'  pfss_trace_field: total time = '+strcompress(ttime,/r)+' min'

;; ;  save a little memory hopefully
;; maxnstep=max(nstep)
;; if (not keyword_set(endpoints)) and (max(nstep) lt stepmax) then begin
;;   ptr=ptr(0:maxnstep-1,*)
;;   ptth=ptth(0:maxnstep-1,*)
;;   ptph=ptph(0:maxnstep-1,*)
;; endif

end
