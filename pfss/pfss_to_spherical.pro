;+
;  NAME: pfss_to_spherical
; 
;  PURPOSE:
;    Given magnetic field data created via the pfss package, this
;    procedure creates a spherical_field_data structure for use with
;    the spherical_* rountines.
; 
;  CALLING SEQUENCE:
;    pfss_to_spherical,sph_data,/free_heap,/no_copy
; 
;  INPUTS:
;    free_heap = if set, will perform a ptr_free operation on all valid
;                pointers before creating a new pointer (useful for avoiding
;                heap variable memory leakage).
;    no_copy = if set, will set the /no_copy keyword when calling ptr_new
;  
;  OUTPUTS: 
;    sph_data = a structure of type spherical_field_data (see
;      spherical_field_data__define.pro)
; 
;  COMMON BLOCKS:
;    uses pfss common block (see pfss_data_block.pro) as input data
; 
;  NOTES:
;    1.  Those variables in the pfss common block for which there is no
;        corresponding field in the sph_data structure are not carried over
;    2.  If /no_copy is set, variables in the pfss_data_block will be
;        undefined afterward.
;
;  MODIFICATION HISTORY:
;    M.DeRosa - 13 Dec 2005 - created
;               27 Jan 2006 - sets sph_data.lonbounds
;                9 Apr 2007 - added no_copy keyword
; 
;-

pro pfss_to_spherical,sph_data,free_heap=free_heap,no_copy=no_copy

;  access pfss common block
@pfss_data_block

;  create output data structure
sph_data={spherical_field_data}

;  create pointers to the various fields
fh=keyword_set(free_heap)
nc=keyword_set(no_copy)
if size(br,/type) gt 0 then begin
  if fh and ptr_valid(sph_data.br) then ptr_free,sph_data.br
  sph_data.br=ptr_new(br,no_copy=nc)
endif
if size(bth,/type) gt 0 then begin
  if fh and ptr_valid(sph_data.bth) then ptr_free,sph_data.bth
  sph_data.bth=ptr_new(bth,no_copy=nc)
endif
if size(bph,/type) gt 0 then begin
  if fh and ptr_valid(sph_data.bph) then ptr_free,sph_data.bph
  sph_data.bph=ptr_new(bph,no_copy=nc)
endif
if size(nr,/type) gt 0 then sph_data.nr=nr
if size(nlat,/type) gt 0 then sph_data.nlat=nlat
if size(nlon,/type) gt 0 then sph_data.nlon=nlon
if size(rix,/type) gt 0 then begin
  if fh and ptr_valid(sph_data.rix) then ptr_free,sph_data.rix
  sph_data.rix=ptr_new(rix,no_copy=nc)
endif
if size(theta,/type) gt 0 then begin
  if fh and ptr_valid(sph_data.theta) then ptr_free,sph_data.theta
  sph_data.theta=ptr_new(theta,no_copy=nc)
endif
if size(phi,/type) gt 0 then begin
  if fh and ptr_valid(sph_data.phi) then ptr_free,sph_data.phi
  sph_data.phi=ptr_new(phi,no_copy=nc)
endif
if size(lat,/type) gt 0 then begin
  if fh and ptr_valid(sph_data.lat) then ptr_free,sph_data.lat
  sph_data.lat=ptr_new(lat,no_copy=nc)
endif
if size(lon,/type) gt 0 then begin
  if fh and ptr_valid(sph_data.lon) then ptr_free,sph_data.lon
  sph_data.lon=ptr_new(lon,no_copy=nc)
endif
if size(str,/type) gt 0 then begin
  if fh and ptr_valid(sph_data.str) then ptr_free,sph_data.str
  sph_data.str=ptr_new(str,no_copy=nc)
endif
if size(stth,/type) gt 0 then begin
  if fh and ptr_valid(sph_data.stth) then ptr_free,sph_data.stth
  sph_data.stth=ptr_new(stth,no_copy=nc)
endif
if size(stph,/type) gt 0 then begin
  if fh and ptr_valid(sph_data.stph) then ptr_free,sph_data.stph
  sph_data.stph=ptr_new(stph,no_copy=nc)
endif
if size(ptr,/type) gt 0 then begin
  if fh and ptr_valid(sph_data.ptr) then ptr_free,sph_data.ptr
  sph_data.ptr=ptr_new(ptr,no_copy=nc)
endif
if size(ptth,/type) gt 0 then begin
  if fh and ptr_valid(sph_data.ptth) then ptr_free,sph_data.ptth
  sph_data.ptth=ptr_new(ptth,no_copy=nc)
endif
if size(ptph,/type) gt 0 then begin
  if fh and ptr_valid(sph_data.ptph) then ptr_free,sph_data.ptph
  sph_data.ptph=ptr_new(ptph,no_copy=nc)
endif
if size(nstep,/type) gt 0 then begin
  if fh and ptr_valid(sph_data.nstep) then ptr_free,sph_data.nstep
  sph_data.nstep=ptr_new(nstep,no_copy=nc)
endif
sph_data.lonbounds=[-1,-1]

end
