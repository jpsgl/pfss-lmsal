;+
;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
;
;  widget_pfss_preview.pro - a widget application for drawing the preview
;                            images associated with the pfss data browser.
;
;  usage:  widgetid=widget_pfss_preview(parent,notify)
;          where parent = widget id of top level base
;                notify = widget id to notify when an event happens
;
;  to do:  -draw outline of projection on lat-lon image
;
;  M.DeRosa - 14 Feb 2003 - created
;             26 Feb 2003 - replaced calls to private routine scim.pro
;                           with inlined image display routines
;             17 Mar 2003 - changed behavior of mouse out-of-bounds events,
;                           also switched values of pb1 and pb2 when tabs are
;                           dragged so that pb1>pb2
;             21 Mar 2003 - moved field line bound boxes from main widget to
;                           this one, along with projection centroid boxes
;              9 May 2003 - added label showing date
;             28 Jan 2004 - changed set_plot,'x' to SSW procedure set_x,
;                           which is Windows- and Mac-proof
;
;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
;-

pro widget_pfss_preview_event,event

; include common block
@pfss_data_block

widget_control,event.id,get_uval=uval
case uval of
  'CANCEL': widget_control,event.top,/destroy
  'EPHEM': begin
    widget_control,event.top,get_uval=state,/no_copy    
    widget_control,state.wmapbcent,set_val=string(b0,f='(f7.3)')
    widget_control,state.wmaplcent,set_val=string(l0,f='(f7.3)')
    widget_control,event.top,set_uval=state,/no_copy    
    draw_preview,event.top
    end
  'DRAW_PREVIEW': draw_preview,event.top
  'DRAW_FIELD': begin

    ;  redraw display, also gets updated values of bounding box fields
    draw_preview,event.top

    ;  launch field widget, or just redraw if already opened
    widget_control,event.top,get_uval=state,/no_copy
    wfield=state.wfieldbase
    widget_control,event.top,set_uval=state,/no_copy
    if not widget_info(wfield,/valid_id) then begin
      wfield=widget_pfss_field(event.top)
    endif else begin
      widget_control,wfield,get_uval=fstate,/no_copy
      linebut=fstate.wlinebut
      widget_control,wfield,set_uval=fstate,/no_copy
      widget_control,linebut,set_uval='TRACE_FIELD', $
        send_event={ID:linebut,TOP:wfield,HANDLER:wfield}
    endelse

    end
  else: stop  ;  SHOULDN'T BE ABLE TO GET HERE !
endcase

end

;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

pro draw_preview,top  ;  (re)draws the display

;  include common block
@pfss_data_block

;  get state
widget_control,top,get_uval=state,/no_copy
widget_control,state.wparent,get_uval=pstate

;  check if centroid has changed
widget_control,state.wmaplcent,get_val=curlon
widget_control,state.wmapbcent,get_val=curlat
viewlon=string(state.plcent,f='(f7.3)')
viewlat=string(state.pbcent,f='(f7.3)')
if (curlon(0) ne viewlon) or (curlat(0) ne viewlat) then begin

  ;  check for range error
  if ((curlon(0) gt 360) or (curlon(0) lt -360)) then begin
    result=dialog_message('Centroid L0 out of range.',/err,dialog_parent=top)
    widget_control,state.wmaplcent,set_val=string(state.plcent,f='(f7.3)')
    widget_control,top,set_uval=state,/no_copy
    return
  endif
  if ((curlat(0) gt 90) or (curlat(0) lt -90)) then begin
    result=dialog_message('Centroid B0 must be between -90 and +90.',/err, $
      dialog_parent=top)
    widget_control,state.wmapbcent,set_val=string(state.pbcent,f='(f7.3)')
    widget_control,top,set_uval=state,/no_copy
    return
  endif

  ;  update state
  state.plcent=(curlon(0)+360.)mod 360
  state.pbcent=curlat(0)

  ;  redraw
  state.imlb=bytscl(rebin(br(*,*,0),nlon*state.pmag,nlat*state.pmag), $
    min=-state.imsc,max=state.imsc,top=248b)
  set_plot,'z'
  device,set_resolution=[nlat*state.pmag,nlat*state.pmag]
  map_set,state.pbcent,state.plcent,/ortho,/iso,/noer,/nobor,pos=[0,0,1,1]
  remap=map_image(br(*,*,0),missing=-state.imsc,/bilinear,/compress, $
    latmin=lat(0),latmax=lat(nlat-1),lonmin=lon(0),lonmax=lon(nlon-1))
  orthoim=bytscl(remap,min=-state.imsc,max=state.imsc,top=248b)
  set_x  ;  used to be set_plot,'x'
  state.imxy=orthoim

endif

;  check if bounding box has changed
widget_control,state.wmapl1,get_val=curl1
widget_control,state.wmapl2,get_val=curl2
widget_control,state.wmapb1,get_val=curb1
widget_control,state.wmapb2,get_val=curb2
viewl1=string(state.pl1,f='(f6.2)')
viewl2=string(state.pl2,f='(f6.2)')
viewb1=string(state.pb1,f='(f6.2)')
viewb2=string(state.pb2,f='(f6.2)')

if (curl1(0) ne viewl1) or (curl2(0) ne viewl2) or $
  (curb1(0) ne viewb1) or (curb2(0) ne viewb2) then begin

  ;  check for range error
  if ((curl1(0) gt 360) or (curl1(0) lt -360)) then begin
    result=dialog_message('L1 out of range.',/err,$
      dialog_parent=top)
    widget_control,top,set_uval=state,/no_copy
    return
  endif
  if ((curl2(0) gt 360) or (curl2(0) lt -360)) then begin
    result=dialog_message('L2 out of range.',/err,$
      dialog_parent=top)
    widget_control,top,set_uval=state,/no_copy
    return
  endif
  if ((curb1(0) gt 90) or (curb1(0) lt -90)) then begin
    result=dialog_message('B1 must be between -90 and +90.',/err,$
      dialog_parent=top)
    widget_control,top,set_uval=state,/no_copy
    return
  endif
  if ((curb2(0) gt 90) or (curb2(0) lt -90)) then begin
    result=dialog_message('B2 must be between -90 and +90.',/err,$
      dialog_parent=top)
    widget_control,top,set_uval=state,/no_copy
    return
  endif

  ;  update state
  state.pl1=(curl1(0)+360.)mod 360
  state.pl2=(curl2(0)+360.)mod 360
  state.pb1=min(double([curb1(0),curb2(0)]))
  state.pb2=max(double([curb1(0),curb2(0)]))

endif

;  (re)draw preview image in window
state.pxsiz=nlon*state.pmag  ;  x size of lon/lat image
state.pysiz=nlat*state.pmag  ;  y size of lon/lat image
wset,state.winprev
tv,state.imlb,0,0
tv,state.imxy,state.pxsiz,0

;  update label
widget_control,state.wdatelabel,set_val='Currently displaying data for '+now

;  compute device location of bounding box hashes
plot,lon,lat,xr=[0,360],yr=[-90,90],xsty=5,ysty=5,/noerase,/nodata,$
  pos=[0,0,state.pxsiz,state.pysiz],/device
coords=convert_coord([state.pl1,state.pl2],[state.pb1,state.pb2],$
  /data,/to_device)
state.pl1d=coords(0,0)  &  state.pl2d=coords(0,1)
state.pb1d=coords(1,0)  &  state.pb2d=coords(1,1)

;  draw bounding box hashes
usersym,[0,0,1],[1,0,0]
plots,state.pl1d,state.pb1d,psym=8,symsize=3,/device
usersym,[1,0,0],[0,0,-1]
plots,state.pl1d,state.pb2d,psym=8,symsize=3,/device
usersym,[0,0,-1],[-1,0,0]
plots,state.pl2d,state.pb2d,psym=8,symsize=3,/device
usersym,[-1,0,0],[0,0,1]
plots,state.pl2d,state.pb1d,psym=8,symsize=3,/device

;  display nicely formatted values in centroid and bbox widgets
widget_control,state.wmaplcent,set_val=string(state.plcent,f='(f7.3)')
widget_control,state.wmapbcent,set_val=string(state.pbcent,f='(f7.3)')
widget_control,state.wmapl1,set_val=string(state.pl1,f='(f6.2)')
widget_control,state.wmapl2,set_val=string(state.pl2,f='(f6.2)')
widget_control,state.wmapb1,set_val=string(state.pb1,f='(f6.2)')
widget_control,state.wmapb2,set_val=string(state.pb2,f='(f6.2)')

;  save state
widget_control,top,set_uval=state,/no_copy

end

;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

pro draw_event,event  ;  events generated by draw widget

;  common block
@pfss_data_block

;  get state
widget_control,event.top,get_uval=state,/no_copy

case 1 of

  event.press: begin

      ;  set up coordinate system
      plot,lon,lat,xr=[0,360],yr=[-90,90],xsty=5,ysty=5,/noerase,/nodata,$
        pos=[0,0,state.pxsiz,state.pysiz],/device

      ;  get x zone
      if state.pl1d gt state.pl2d then begin  ;  wrapped box
        dx=state.pl2d-state.pl1d+state.pxsiz  ;  box size in pixels
        if event.x lt ((state.pl1d+state.pl2d)/2) then $
          event.x=event.x+state.pxsiz
        case 1 of
          (event.x lt (state.pl1d+round(dx/3.))): xzon=0
          (event.x gt (state.pl1d+round(2.*dx/3.))): xzon=2
          else: xzon=1
        endcase
      endif else begin  ;  normal box
        dx=state.pl2d-state.pl1d  ;  box size in pixels
        case 1 of
          (event.x lt (state.pl1d+round(dx/3.))): xzon=0
          (event.x gt (state.pl1d+round(2.*dx/3.))): xzon=2
          else: xzon=1
        endcase
      endelse

      ;  get y zone
      dy=state.pb2d-state.pb1d  ;  box size in pixels
      case 1 of
        (event.y lt (state.pb1d+round(dy/3.))): yzon=0
        (event.y gt (state.pb1d+round(2.*dy/3.))): yzon=2
        else: yzon=1
      endcase

      ;  set state
      state.mmousedn=1b
      state.pzone=xzon+3*yzon

      ;  save state
      widget_control,event.top,set_uval=state,/no_copy

      end

  event.release: begin

      ;  flag mouse as up
      state.mmousedn=0b

      ;  change state depending on zone
      if state.pzone eq 4 then begin

        ;  get mouse up position in degrees
        coords=convert_coord(event.x,event.y,/device,/to_data)

        ;  x coordinate
        if state.pl1d gt state.pl2d then dxh=(state.pl2-state.pl1+360)/2. $
          else dxh=(state.pl2-state.pl1)/2.
        widget_control,state.wmapl1, $
          set_val=string((coords(0)-dxh+360.)mod 360,f='(f6.2)')
        widget_control,state.wmapl2, $
          set_val=string((coords(0)+dxh+360.)mod 360,f='(f6.2)')

        ;  y coordinate
        dyh=(state.pb2-state.pb1)/2.
        case 1 of
          (coords(1)-dyh) le -90: begin
            widget_control,state.wmapb1,set_val=string(-90,f='(f6.2)')
            widget_control,state.wmapb2,set_val=string(-90+2.*dyh,f='(f6.2)')
            end
          (coords(1)+dyh) ge 90: begin
            widget_control,state.wmapb1,set_val=string(90-2.*dyh,f='(f6.2)')
            widget_control,state.wmapb2,set_val=string(90,f='(f6.2)')
            end
          else: begin
            widget_control,state.wmapb1, $
              set_val=string(coords(1)-dyh,f='(f6.2)')
            widget_control,state.wmapb2, $
              set_val=string(coords(1)+dyh,f='(f6.2)')
            end
        endcase
      endif else begin
        case state.pzone of
          0: begin
             state.pl1d=event.x  &  state.pb1d=event.y>0
             end
          1: state.pb1d=event.y>0
          2: begin
             state.pl2d=event.x  &  state.pb1d=event.y>0
             end
          3: state.pl1d=event.x
          5: state.pl2d=event.x
          6: begin
             state.pl1d=event.x  &  state.pb2d=event.y<state.pysiz
             end
          7: state.pb2d=event.y<state.pysiz
          8: begin
             state.pl2d=event.x  &  state.pb2d=event.y<state.pysiz
             end
          else:  stop  ;  SHOULDN'T BE ABLE TO GET HERE !
        endcase

        ;  convert coords
        coords=convert_coord([state.pl1d,state.pl2d],[state.pb1d,state.pb2d],$
          /device,/to_data)
        widget_control,state.wmapl1, $
          set_val=string((coords(0,0)+360.)mod 360,f='(f6.2)')
        widget_control,state.wmapl2, $
          set_val=string((coords(0,1)+360.)mod 360,f='(f6.2)')
        widget_control,state.wmapb1,set_val=string(min(coords(1,*)),f='(f6.2)')
        widget_control,state.wmapb2,set_val=string(max(coords(1,*)),f='(f6.2)')

      endelse

      ;  save state and redraw
      widget_control,event.top,set_uval=state,/no_copy
      draw_preview,event.top

      end

  else: begin

      ;  return if mouse not down
      if not state.mmousedn then begin
        widget_control,event.top,set_uval=state,/no_copy
        return
      endif

      ;  change state depending on zone
      if state.pzone eq 4 then begin

        ;  get mouse up position in degrees
        coords=convert_coord(event.x,event.y,/device,/to_data)

        ;  x coordinate
        if state.pl1d gt state.pl2d then dxh=(state.pl2-state.pl1+360)/2. $
          else dxh=(state.pl2-state.pl1)/2.
        widget_control,state.wmapl1, $
          set_val=string((coords(0)-dxh+360.)mod 360,f='(f6.2)')
        widget_control,state.wmapl2, $
          set_val=string((coords(0)+dxh+360.)mod 360,f='(f6.2)')

        ;  y coordinate
        dyh=(state.pb2-state.pb1)/2.
        case 1 of
          (coords(1)-dyh) le -90: begin
            widget_control,state.wmapb1,set_val=string(-90,f='(f6.2)')
            widget_control,state.wmapb2,set_val=string(-90+2.*dyh,f='(f6.2)')
            end
          (coords(1)+dyh) ge 90: begin
            widget_control,state.wmapb1,set_val=string(90-2.*dyh,f='(f6.2)')
            widget_control,state.wmapb2,set_val=string(90,f='(f6.2)')
            end
          else: begin
            widget_control,state.wmapb1, $
              set_val=string(coords(1)-dyh,f='(f6.2)')
            widget_control,state.wmapb2, $
              set_val=string(coords(1)+dyh,f='(f6.2)')
            end
        endcase

      endif else begin
        case state.pzone of
          0: begin
             state.pl1d=event.x  &  state.pb1d=event.y>0
             end
          1: state.pb1d=event.y>0
          2: begin
             state.pl2d=event.x  &  state.pb1d=event.y>0
             end
          3: state.pl1d=event.x
          5: state.pl2d=event.x
          6: begin
             state.pl1d=event.x  &  state.pb2d=event.y<state.pysiz
             end
          7: state.pb2d=event.y<state.pysiz
          8: begin
             state.pl2d=event.x  &  state.pb2d=event.y<state.pysiz
             end
          else:  stop  ;  SHOULDN'T BE ABLE TO GET HERE !
        endcase

        ;  convert coords
        coords=convert_coord([state.pl1d,state.pl2d],[state.pb1d,state.pb2d],$
          /device,/to_data)
        widget_control,state.wmapl1, $
          set_val=string((coords(0,0)+360.)mod 360,f='(f6.2)')
        widget_control,state.wmapl2, $
          set_val=string((coords(0,1)+360.)mod 360,f='(f6.2)')
        if coords(1,0) ge coords(1,1) then begin
          widget_control,state.wmapb1,set_val=string(coords(1,1),f='(f6.2)')
          widget_control,state.wmapb2,set_val=string(coords(1,0),f='(f6.2)')
          case state.pzone of
            0: state.pzone=6
            1: state.pzone=7
            2: state.pzone=8
            6: state.pzone=0
            7: state.pzone=1
            8: state.pzone=2
            else:  stop  ;  SHOULDN'T BE ABLE TO GET HERE !
          endcase
        endif else begin
          widget_control,state.wmapb1,set_val=string(coords(1,0),f='(f6.2)')
          widget_control,state.wmapb2,set_val=string(coords(1,1),f='(f6.2)')
        endelse

      endelse

      ;  save state and redraw
      widget_control,event.top,set_uval=state,/no_copy
      draw_preview,event.top

      end

endcase

end

;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

function widget_pfss_preview,wparent,wnotify

;  common block
@pfss_data_block

;  set defaults
mag=2  ;  magnification factor for preview data
imsc=400  ;  image scaling

;  construct widget
master=widget_base(/column,title='PFSS previewer',group=wparent,$
  /tlb_frame_attr,/align_center)
titlebase=widget_base(master,/row,/align_right)
buttons=widget_base(titlebase,/col,/align_center)
cancelbut=widget_button(buttons,val='  Close window  ',uval='CANCEL')
redrawbut=widget_button(buttons,val=' Update preview ',uval='DRAW_PREVIEW')
flinesbut=widget_button(buttons,val='Draw field lines',uval='DRAW_FIELD')

;  bounding box L and B subwidget
wbboxlb=widget_base(titlebase,/col,frame=5,/base_align_center)
boxlblabel=widget_label(wbboxlb,val='lon/lat bounds (deg):')
boxlb1base=widget_base(wbboxlb,/row)
boxlon1label=widget_label(boxlb1base,val='L1:')
boxlon1text=widget_text(boxlb1base,uval='DRAW_PREVIEW',xsiz=6,/edit,font='Courier')
boxlat1label=widget_label(boxlb1base,val=' B1:')
boxlat1text=widget_text(boxlb1base,uval='DRAW_PREVIEW',xsiz=6,/edit,font='Courier')
boxlb2base=widget_base(wbboxlb,/row)
boxlon2label=widget_label(boxlb2base,val='L2:')
boxlon2text=widget_text(boxlb2base,uval='DRAW_PREVIEW',xsiz=6,/edit,font='Courier')
boxlat2abel=widget_label(boxlb2base,val=' B2:')
boxlat2text=widget_text(boxlb2base,uval='DRAW_PREVIEW',xsiz=6,/edit,font='Courier')

;  bounding box X and Y subwidget
wbboxxy=widget_base(titlebase,/col,frame=5,/base_align_center)
boxxylabel=widget_label(wbboxxy,val='X/Y bounds (normalized):')
boxxy1base=widget_base(wbboxxy,/row)
boxx1label=widget_label(boxxy1base,val='X1:')
boxx1text=widget_text(boxxy1base,uval='DRAW_PREVIEW',xsiz=6,/edit,font='Courier')
boxy1label=widget_label(boxxy1base,val=' Y1:')
boxy1text=widget_text(boxxy1base,uval='DRAW_PREVIEW',xsiz=6,/edit,font='Courier')
boxxy2base=widget_base(wbboxxy,/row)
boxx2label=widget_label(boxxy2base,val='X2:')
boxx2text=widget_text(boxxy2base,uval='DRAW_PREVIEW',xsiz=6,/edit,font='Courier')
boxy2abel=widget_label(boxxy2base,val=' Y2:')
boxy2text=widget_text(boxxy2base,uval='DRAW_PREVIEW',xsiz=6,/edit,font='Courier')

;  projection centroid widget
widmapcent=widget_base(titlebase,/col,frame=5)
viewtitle=widget_label(widmapcent,val='Projection centroid (deg):')
viewbase=widget_base(widmapcent,/row)
viewlonlabel=widget_label(viewbase,val='L0:')
viewlontext=widget_text(viewbase,uval='DRAW_PREVIEW',xsiz=7,/edit,$
  val=string(l0,f='(f7.3)'),font='Courier')
viewlatlabel=widget_label(viewbase,val=' B0:')
viewlattext=widget_text(viewbase,uval='DRAW_PREVIEW',xsiz=7,/edit,$
  val=string(b0,f='(f7.3)'),font='Courier')
viewephbut=widget_button(widmapcent,val=' Reset to Earth L0,B0 ', $
  uval='EPHEM', /align_center)

;  date info
datelabel=widget_label(master,val='Currently displaying data for '+now)

drawprev=widget_draw(master,retain=2,xsiz=nlat*3*mag,ysiz=nlat*mag,/button,/motion,event_pro='draw_event')

;  realize
widget_control,master,/realize
widget_control,drawprev,get_val=winprev  ;  only assigned after realization
xmanager,'widget_pfss_preview',master,/no_block

;  get preview images
latlonim=bytscl(rebin(br(*,*,0),nlon*mag,nlat*mag),min=-imsc,max=imsc,top=248b)
set_plot,'z'
device,set_resolution=[nlat*mag,nlat*mag]
map_set,b0,l0,/ortho,/iso,/noer,/nobor,pos=[0,0,1,1]
remap=map_image(br(*,*,0),missing=-imsc,/bilinear,/compress, $
  latmin=lat(0),latmax=lat(nlat-1),lonmin=lon(0),lonmax=lon(nlon-1))
orthoim=bytscl(remap,min=-imsc,max=imsc,top=248b)
set_x  ;  used to be set_plot,'x'

;  get color table (really only needs to be done once)
re=[bindgen(250),0b,0b,255b,255b,255b,255b]
gr=[bindgen(250),255b,255b,0b,0b,255b,255b]
bl=[bindgen(250),0b,0b,255b,255b,255b,255b]
tvlct,re,gr,bl

;  set wpreview field in main pfss_viewer state structure
widget_control,wparent,get_uval=pstate,/no_copy
pstate.wprevbase=master
pstate.winpreview=winprev
pstate.wpreview=drawprev
widget_control,wparent,set_uval=pstate,/no_copy

;  save state structure (w=widget, win=window, p=parameter, m=mouse, im=image)
state={wparent:wparent, wpreview:drawprev, wfieldbase:-1l, $
  wmaplcent:viewlontext, wmapbcent:viewlattext, wdatelabel:datelabel, $
    wmapl1:boxlon1text, wmapl2:boxlon2text, $
    wmapb1:boxlat1text, wmapb2:boxlat2text, $
  wcancel:cancelbut, wredraw:redrawbut, $
  winprev:winprev, winpix:-1l, mmousedn:0b, pxsiz:-1l, pysiz:-1l, pmag:mag, $
  plcent:l0, pbcent:b0, pl1d:-1l, pl2d:-1l, pb1d:-1l, pb2d:-1, pzone:-1l, $
  pl1:0., pl2:0., pb1:0., pb2:0., px1:-0.3, px2:0.3, py1:-0.3, py2:0.3, $
  imsc:imsc, imlb:latlonim, imxy:orthoim }

;  compute and display location of bounding box hashes
state.pb1=b0(0)-30
state.pb2=b0(0)+30
state.pl1=(l0(0)+360-30)mod 360
state.pl2=(l0(0)+360+30)mod 360
widget_control,state.wmapl1,set_val=string(state.pl1,f='(f6.2)')
widget_control,state.wmapl2,set_val=string(state.pl2,f='(f6.2)')
widget_control,state.wmapb1,set_val=string(state.pb1,f='(f6.2)')
widget_control,state.wmapb2,set_val=string(state.pb2,f='(f6.2)')

;  save state and display
widget_control,master,set_uval=state,/no_copy
draw_preview,master

return,master

end
