;+
;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
;
;  pfss_field_start_coord - This chooses gridpoints from which to trace the
;                           magnetic field lines we will later visualize
;
;  usage:  pfss_field_start_coord,fieldtype,spacing,radstart=radstart,/top,
;            bbox=bbox,/add
;          where fieldtype = 1=starting points fall along the equator
;                            2=uniform grid, with a random offset
;                            3=points are distributed randomly in both
;                              latitude and longitude
;                            4=read in from a file
;                            5=uniform grid (default)
;                            6=points are weighted by flux
;                            7=rectangular grid
;                spacing = parameter controlling density of points, does
;                          different things depending on fieldtype
;                radstart = a scalar radius at which all of the fieldilne
;                           starting points will be set (default = minimum
;                           radius in the domain)
;                top = set if field lines are to start at upper radius rather
;                      than lower radius (setting this flag is the same as
;                      setting radstart to the maximum radius in the domain)
;                bbox = [lon1,lat1,lon2,lat2] defining bounding box in degrees
;                       outside of which no field line starting point can lie
;                add = set this flag if the new starting points are to be
;                      added to an existing set already stored in the common
;                      block (default is to overwrite)
;
;          and in the common block:
;                (str,stth,stph) = on output, coordinates of each point
;
;  notes: -fieldtype=4 is not yet implemented
;
;  M.DeRosa -  8 Feb 2002 - converted from an earlier script
;             23 May 2002 - now recognizes if domain is spherical segment
;             23 May 2002 - added fieldtype 6
;             12 Aug 2002 - added bbox keyword
;             12 May 2003 - changed calling sequence slightly, now puts
;                           str,stth,stph directly into common block
;                           eliminating need to have them as
;                           parameters
;             20 Sep 2003 - changed counters to long integers
;             18 Jan 2005 - bbox keyword works with fieldtype=5
;             16 Dec 2005 - added radstart,add keywords
;             16 Dec 2005 - standardized the fieldtype options so that the
;                           bbox keyword works with all of them
;             19 Dec 2005 - now handles "wraparound" bounding boxes,
;                           i.e. bounding boxes that span the 0 meridian
;             19 Aug 2006 - routine is now merely a wrapper for the 
;                           spherical_field_start_coord routine
;              5 Feb 2007 - added simple error check
;             14 Dec 2009 - adjusted error check as fieldtype now goes up to 7
;             20 May 2010 - fixed bug when /top is used with /add
;
;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
;-

pro pfss_field_start_coord,fieldtype,spacing,top=top,_extra=exkey

;  print usage message
if n_params() eq 0 then begin
  print,'  pfss_field_start_coord,fieldtype,spacing,radstart=radstart,/top,bbox=bbox,/add'
  return
endif

;  include common block
@pfss_data_block

;  preliminaries
if n_elements(fieldtype) eq 0 then fieldtype=5 else fieldtype=round(fieldtype)
if (fieldtype lt 1) or (fieldtype gt 7) then fieldtype=5

;  simple error check: check to make sure field is defined
if (n_elements(br) eq 0) or (n_elements(bth) eq 0) or (n_elements(bph) eq 0) $
  then begin
  print,'  pfss_field_start_coord: ERROR - br,bth,bph are not defined correctly'
  return
endif

;  convert to spherical_field_data structure
pfss_to_spherical,pfss_data,/free,/no_copy

;  call sister routine
if keyword_set(top) then begin
  spherical_field_start_coord,pfss_data,fieldtype,spacing,$
    radstart=max(*pfss_data.rix),_extra=exkey
endif else begin
  spherical_field_start_coord,pfss_data,fieldtype,spacing,_extra=exkey
endelse

;  convert back to pfss common block
spherical_to_pfss,pfss_data,/noreset,/no_copy

;  OLD STUFF IS BELOW - commented out on 8/19/2006

; ;  get rmin,rmax    
; rmin=min(rix,max=rmax)

; ;  set radstart if not set
; if n_elements(radstart) eq 0 then radstart=rmin else radstart=radstart(0)
; if keyword_set(top) then radstart=rmax

; ;  deal with add keyword
; if keyword_set(add) then begin
;   if n_elements(str) gt 0 then begin
;     strold=str
;     stthold=stth
;     stphold=stph
;   endif
; endif

; case fieldtype of

;   1: begin  ;  simple arrangements of starting points

;     npt=round(spacing(0))  ;  spacing = number of points
;     str=replicate(radstart,npt)
;     stth=replicate(!dpi/2,npt)
;     if n_elements(bbox) eq 2 then begin
;       phmin=bbox(0)
;       phmax=bbox(1)
;       if phmax lt phmin then phmax=phmax+360
;       stph=linrange(npt,phmin,phmax)
;       stph=((stph+360) mod 360) * !dpi/180
;     endif else begin
;       stph=(linrange(npt+1,0,360))(0:npt-1)
;       stph=stph*!dpi/180
;     endelse
     
;     end

;   2: begin  ;  uniform grid, with a random offset

;     ;  set up binning
;     nlatbin=round(double(nlat)/spacing(0))
;     dlatbin=!dpi/nlatbin
;     latbin=linrange(nlatbin,dlatbin/2,!dpi-dlatbin/2)
;     nlonbin=round(nlatbin*2*sin(latbin))
;     dlonbin=2*!dpi/nlonbin
;     nloncum=lonarr(nlatbin)
;     for i=0l,nlatbin-1 do nloncum(i)=total(nlonbin(0:i))
;     npt=round(total(nlonbin))  ;  total number of points

;     ;  add random offsets
;     stth=randomu(seed,npt)-0.5
;     stph=randomu(seed,npt)-0.5
;     for i=0l,npt-1 do begin
;       lonbinix=(where(i lt nloncum))(0)
;       stth(i)=latbin(lonbinix)+stth(i)*dlatbin/2
;       stph(i)=(i-(nloncum(lonbinix)-nlonbin(lonbinix)))*dlonbin(lonbinix) + $
;         stph(i)*dlonbin(lonbinix)/2
;     endfor
;     stph=(stph+2*!dpi) mod (2*!dpi)
    
;     if n_elements(bbox) eq 4 then begin  ;  remove all points outside box

;       ;  filter in latitude
;       whlat=where((stth ge (90-bbox(3))*!dpi/180) $
;         and (stth le (90-bbox(1))*!dpi/180),nwh)
;       if nwh gt 0 then begin
;         stth=stth(whlat)
;         stph=stph(whlat)
;       endif

;       ;  filter in longitude
;       lon1=((bbox(0)+360) mod 360)*!dpi/180
;       lon2=((bbox(2)+360) mod 360)*!dpi/180
;       if (lon2 gt lon1) then begin
;         whlon=where((stph ge lon1) and (stph le lon2),nwh)
;       endif else begin
;         whlon=where((stph le lon2) or (stph ge lon1),nwh)
;       endelse
;       if nwh gt 0 then begin
;         stth=stth(whlon)
;         stph=stph(whlon)
;       endif

;     endif

;     ;  set radial starting points
;     str=replicate(radstart,n_elements(stth))

;     end

;   3: begin  ;  choose locations at random

;     npt=round(spacing(0))  ;  number of points
;     str=replicate(radstart,npt)
;     if n_elements(bbox) eq 4 then begin
;       thmin=(90-bbox(3))*!dpi/180
;       thmax=(90-bbox(1))*!dpi/180
;       stth=randomu(seed,npt)*(thmax-thmin)+thmin
;       phmin=bbox(0)
;       phmax=bbox(2)
;       if phmax lt phmin then phmax=phmax+360
;       stph=((randomu(seed,npt)*(phmax-phmin)+phmin) mod 360) * !dpi/180
;     endif else begin
;       thmin=min(theta,max=thmax)
;       stth=randomu(seed,npt)*(thmax-thmin)+thmin
;       stph=randomu(seed,npt)*2*!dpi
;     endelse

;     end

;   4: begin  ;  read in locations from a file

;     print,'  pfss_field_start_coord: fieldtype=4 not implemented'
;     return

;     end

;   5: begin  ;  uniform grid

;     ;  set up binning
;     nlatbin=round(double(nlat)/spacing(0))
;     dlatbin=!dpi/nlatbin
;     latbin=linrange(nlatbin,dlatbin/2,!dpi-dlatbin/2)
;     nlonbin=round(nlatbin*2*sin(latbin))
;     nloncum=lonarr(nlatbin)
;     for i=0l,nlatbin-1 do nloncum(i)=total(nlonbin(0:i))
;     npt=round(total(nlonbin))  ;  total number of points

;     ;  choose starting points
;     stth=dblarr(npt,/noz)
;     stph=dblarr(npt,/noz)
;     for i=0l,npt-1 do begin
;       latbinix=(where(i lt nloncum))(0)
;       stth(i)=latbin(latbinix)
;       stph(i)=(nloncum(latbinix)-i)*2*!dpi/nlonbin(latbinix)-dlatbin/2
;     endfor

;     if keyword_set(bbox) then begin  ;  remove all points outside bbox

;       ;  filter in latitude
;       whlat=where((stth ge (90-bbox(3))*!dpi/180) $
;               and (stth le (90-bbox(1))*!dpi/180),nwh)
;       if nwh gt 0 then begin
;         stth=stth(whlat)
;         stph=stph(whlat)
;       endif

;       ;  filter in longitude        
;       lon1=((bbox(0)+360) mod 360)*!dpi/180
;       lon2=((bbox(2)+360) mod 360)*!dpi/180
;       if (lon2 gt lon1) then begin
;         whlon=where((stph ge lon1) and (stph le lon2),nwh)
;       endif else begin
;         whlon=where((stph le lon2) or (stph ge lon1),nwh)
;       endelse
;       if nwh gt 0 then begin
;         stth=stth(whlon)
;         stph=stph(whlon)
;       endif

;     endif

;     ;  set radial starting points
;     str=replicate(radstart,n_elements(stth))

;     end

;   6:  begin  ;  flux-based

;     ;  get random starting points, oversample by a factor of 10
;     npt=round(spacing(0))
;     oversampling=10l  ;  oversampling factor
;     if n_elements(bbox) eq 4 then begin
;       thmin=(90-bbox(3))*!dpi/180
;       thmax=(90-bbox(1))*!dpi/180
;       stth=randomu(seed,npt*oversampling)*(thmax-thmin)+thmin
;       phmin=bbox(0)*!dpi/180
;       phmax=bbox(2)*!dpi/180
;       stph=randomu(seed,npt*oversampling)*(phmax-phmin)+phmin
;     endif else begin
;       thmin=min(theta,max=thmax)
;       stth=randomu(seed,npt*oversampling)*(thmax-thmin)+thmin
;       stph=randomu(seed,npt*oversampling)*2*!dpi
;     endelse
;     str=replicate(radstart,npt*oversampling)

;     ;  get br at the starting radius of the random points
;     ir=get_interpolation_index(rix,str)
;     ith=get_interpolation_index(lat,90-stth*180/!dpi)
;     iph=get_interpolation_index(lon,stph*180/!dpi)
;     br0=interpolate(br,iph,ith,ir)

;     ;  reverse sort b values (should we take into account the unequal areas?)
;     magix=reverse(sort(abs(br0)))
;     str=str(magix(0:npt-1))
;     stth=stth(magix(0:npt-1))
;     stph=stph(magix(0:npt-1))

;     end

;   else: print,'  pfss_field_start_coord:  invalid fieldtype'

; endcase

; if keyword_set(top) then str(*)=rmax

; ;  finish dealing with add keyword
; if keyword_set(add) then begin
;   if n_elements(strold) gt 0 then begin
;     str=[strold,str]
;     stth=[stthold,stth]
;     stph=[stphold,stph]
;   endif
; endif

end

