;+
;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
;
;  pfss_get_potl_coeffs.pro - Computes spherical harmonic coefficients of
;                             field potential in (l,m,r)-space from a map of
;                             the radial magnetic field located at r=1
;
;  usage:  pfss_get_potl_coeffs,mag,rtop=rtop,/quiet
;       where mag = input magnetogram
;             rtop = if source surface upper BC is desired, set to the radius
;                    of source surface (assuming input magnetogram is located 
;                    at r=1), otherwise field spans 1<r<infinity 
;             quiet = set keyword to disable screen output
;
;       and in the common block we have:
;             phiat=on output, (l,m) array of dcomplex coeffs, 
;                   corresponding to r^l eigenfunction
;             phibt=on output, (l,m) array of dcomplex coeffs,
;                   corresponding to 1/r^(l+1) eigenfunction
;
;  M.DeRosa - 30 Jan 2002 - converted from earlier script
;             12 May 2003 - converted common block to PFSS package format
;             12 May 2004 - added check for overflow numbers when computing
;                           coefficients 
;
;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
;-

pro pfss_get_potl_coeffs,mag,rtop=rtop,quiet=quiet

;  print usage message
if n_params() eq 0 then begin
  print,'  pfss_get_potl_coeffs,mag,rtop=rtop,/quiet'
  return
endif

;  include common block
@pfss_data_block

;  preliminaries
cth=cos(theta)
sth=sqrt(1-cth*cth)
nlat=n_elements(cth)
nlon=2*nlat
nax=size(mag,/dim)
if nlon ne nax(0) then begin
  print,'  ERROR in pfss_get_potl_coeffs:  nlon is off'
  return
endif
if nlat ne nax(1) then begin
  print,'  ERROR in pfss_get_potl_coeffs:  nlat is off'
  return
endif

;  get spherical harmonic transform of magnetogram
lmax=nlat
magt=spherical_transform(mag,cth,lmax=lmax)

;  get l and m index arrays of transform
lix=lindgen(lmax+1)
mix=lix
larr=lix#replicate(1,lmax+1)
marr=replicate(1,lmax+1)#mix
wh=where(marr gt larr)
larr(wh)=0  &  marr(wh)=0

;  determine coefficients
if keyword_set(rtop) then begin  ;  source surface at rtop
  rtop=double(rtop(0))
  phibt=-magt/(1+larr*(1+rtop^(-2*larr-1)))
  phiat=-phibt/(rtop^(2*larr+1))
  wh=where(finite(phiat) eq 0b,nwh)
  if nwh gt 0 then phiat(wh)=complex(0,0)
endif else begin  ;  potential field extends to infinity, all A's are 0
  phiat=magt  &  phiat(*)=0
  phibt=-magt/(1+larr)
endelse

if not keyword_set(quiet) then $
  print,'  pfss_get_potl_coeffs:  forward transform completed'

end
