;+
; NAME: pfss_view_create.pro
;
; PURPOSE: To create a view object containing a PFSS magnetic field model
;
; CALLING SEQUENCE:
;   pfss_view_create,object_list
;
; INPUTS: None specified explicitly, but from the common block we have
;                br = r-component of magnetic field
;                (ptr,ptth,ptph) = on input, contains a (n,stepmax)-array of
;                                  field line coordinates
;                nstep = an n-vector (where n=number of field lines) 
;                        containing the number of points comprising each 
;                        field line
;                rimage = on output, image of z=buffer is read into this
;                         variable
;
; OUTPUTS:
;   object_list = on output, contains a structure containing the objects
;                 created by this routine
;
; NOTES: -Object is created such that the viewer is looking down at the north
;         pole, with 0 degrees longitude pointing to the right.
;        -Once a valid PFSS model has been loaded, and some fieldilnes have
;         been traced, for simple testing at the IDL prompt type:
;           pfss_view_create,olist
;           owindow=obj_new('IDLgrWindow',graph=olist.oview,dim=[400,400])
;           owindow->draw
;        -To rotate the view to the ephemeral values before drawing, add the
;         following four lines before drawing the view:
;           olist.omodelin->rotate,[1,0,0],-90  ;  point rotation axis up
;           olist.omodelin->rotate,[0,1,0],-90  ;  rotate long=0 to front
;           olist.omodelin->rotate,[0,1,0],-l0
;           olist.omodelin->rotate,[1,0,0],b0
;
; MODIFICATION HISTORY: 
;   M.DeRosa - 22 Aug 2006 - created
;
;-

pro pfss_view_create,object_list

;  first create spherical_field_data_structure
pfss_to_spherical,pfss_data,/free

;  set some RGB triplets of line colors
bla=byte([  0,  0,  0])
yel=byte([255,255,  0])
gre=byte([  0,255,  0])
red=byte([255,  0,255])
whi=byte([255,255,255])

;  determine rmin and rmax and thmin and thmax
rmax=max(*pfss_data.rix,min=rmin)
thmin=min(*pfss_data.theta,max=thmax)

;  create view object
oview=obj_new('IDLgrView',color=bla,proj=1,zclip=[0.1,-(2*rmax+0.1)], $
  eye=2*rmax+1,viewplane=[-1,-1,2,2]*rmax)

;  create the outer model, and move it so that the front coincides with z=0
omodeltop=obj_new('IDLgrModel')
oview->add,omodeltop
omodeltop->translate,0,0,-rmax

;  create the inner model, and rotate it to a sensible position
omodelin=obj_new('IDLgrModel')
omodeltop->add,omodelin
omodelin->rotate,[1,0,0],-90  ;  point rotation axis up
omodelin->rotate,[0,1,0],-90  ;  rotate so that long=0 is at front

;  create fieldline objects
nlines=n_elements(*pfss_data.nstep)
open=intarr(nlines)
firstline=1
for i=0,nlines-1 do begin

  ;  only draw lines that have line data
  ns=(*pfss_data.nstep)(i)
  if ns gt 0 then begin

    ;  determine whether field lines are open or closed
    if (max((*pfss_data.ptr)(0:ns-1,i))-rmin)/(rmax-rmin) gt 0.99 then begin
      irc=get_interpolation_index(*pfss_data.rix,(*pfss_data.ptr)(0,i))
      ithc=get_interpolation_index( $
        *pfss_data.lat,90-(*pfss_data.ptth)(0,i)*!radeg)
      iphc=get_interpolation_index( $
        *pfss_data.lon,((*pfss_data.ptph)(0,i)*!radeg+360) mod 360)
      brc=interpolate(*pfss_data.br,iphc,ithc,irc)
      if brc gt 0 then open(i)=1 else open(i)=-1
    endif  ;  else open(i)=0, which has already been done

    ;  flag those lines that go higher than the first radial gridpoint
    heightflag=max((*pfss_data.ptr)(0:ns-1,i)) gt (*pfss_data.rix)(1)

    ;  create an object for this line and add it to the model
    if heightflag then begin

      ;  set appropriate color
      case open(i) of
        -1: col=red
         0: col=whi
         1: col=gre
      endcase

      ;  transform from spherical to cartesian coordinates
      linecoords=cv_coord(/to_rect,from_sph=transpose( $
        [[(*pfss_data.ptph)(0:ns-1,i)],[!dpi/2-(*pfss_data.ptth)(0:ns-1,i)], $
        [(*pfss_data.ptr)(0:ns-1,i)]]))

      ;  create polyline object
      flobj=obj_new('IDLgrPolyline',linecoords,color=col,thick=thick, $
        xcoord_conv=scales,ycoord_conv=scales,zcoord_conv=scales)
      if firstline then begin
        ofieldlines=flobj
        firstline=0
      endif else ofieldlines=[ofieldlines,flobj]

    endif

  endif

endfor

;  add fieldlines to model
if n_elements(ofieldlines) gt 0 then omodelin->add,ofieldlines

;  create structure of objects
object_list={oview:oview,omodeltop:omodeltop,omodelin:omodelin,$
  ofieldlines:ofieldlines}

end
