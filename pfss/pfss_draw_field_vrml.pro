;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
;
;  pfss_draw_field_vrml.pro  - This procedure creates a VRML rendering of a 
;                              field line extrapolation
;  usage:  pfss_draw_field_vrml,bcent=bcent,lcent=lcent,imsc=imsc,file=file,
;                subsample=subsample,/extrude,/quiet
;          where (bcent,lcent) = central (lat,lon) in degrees of centroid of
;                                projection (default = (0,0))
;                imsc = data value(s) to which to scale central magnetogram 
;                       image (default = min/max of image)
;                file = name of VRML file, (default = 'test')
;                subsample = factor by which to subsample the number of points
;                            used to define each fieldline
;                extrude = set to render each field line as a tube
;                quiet = if set, disables screen output
;
;          and in the common block we have
;                br = r-component of magnetic field
;                (ptr,ptth,ptph) = on input, contains a (n,stepmax)-array of
;                                  field line coordinates
;                nstep = an n-vector (where n=number of field lines) 
;                        containing the number of points comprising each 
;                        field line
;
;  notes:  - does not yet work for spherical segments, only full spheres
;
;  M.DeRosa - 19 Nov 2002 - created, adapted from draw_field.pro
;              4 Jun 2003 - added to PFSS package
;             14 Oct 2003 - resolved conflict with SSW get_pid process
;             28 Jan 2004 - changed set_plot,'x' to SSW procedure set_x,
;                           which is Windows- and Mac-proof
;             19 Apr 2006 - changed set_x command to set_plot,olddname, where
;                           olddname is the device name contained in !d.name
;                           upon entry to this routine
;             10 Dec 2011 - discovered that texture map for VRML was inverted
;                           in the latitudinal dimension
;  F.Breitling - 16 Feb 2016 - added subsample keyword, and changed some file
;                              naming conventions, writes central image in a
;                              PNG file instead of a TIFF, and does not use
;                              the process ID anymore in the filename
;
;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

pro pfss_draw_field_vrml,bcent=bcent,lcent=lcent,imsc=imsc,file=file, $
  subsample=subsample,extrude=extrude,quiet=quiet

;  include common block
@pfss_data_block

;  some error checking
if n_elements(bcent) eq 0 then bcent=0
if n_elements(lcent) eq 0 then lcent=0
if n_elements(file) eq 0 then file='test'
if n_elements(subsample) gt 0 then subsample=(float(subsample(0)))>1.0

;  preliminaries
cb=cos(bcent*!dtor)
sb=-sin(bcent*!dtor)
rmin=min(rix,max=rmax)
top=abs(ptr(0)-rmax) lt abs(ptr(0)-rmin)
npt=n_elements(nstep)  ;  npt = number of field lines to be drawn

;  VRML color table (equivalent to my color table 47)
olddname=!d.name  &  set_plot,'z'
re=[bindgen(250),0b,0b,255b,255b,255b,255b]
gr=[bindgen(250),255b,255b,0b,0b,255b,255b]
bl=[bindgen(250),0b,0b,255b,255b,255b,255b]
blu=string(re(252)/255.,gr(252)/255.,bl(252)/255.,f='(3f5.2)')
gre=string(re(250)/255.,gr(250)/255.,bl(250)/255.,f='(3f5.2)')
whi=string(re(254)/255.,gr(254)/255.,bl(254)/255.,f='(3f5.2)')
bla=string(re(0)/255.,gr(0)/255.,bl(0)/255.,f='(3f5.2)')

;  open VRML file
openw,lun,file+'.wrl',/get

;  write header
printf,lun,'#VRML V2.0 utf8'
printf,lun
printf,lun,'WorldInfo {'
printf,lun,'  title "VRML field lines" }'
printf,lun

;  set up entry viewpoint
printf,lun,'Viewpoint { position 0.0 0.0 10.0 description "Entry View" }'
printf,lun

;  set up north pole viewpoint
printf,lun,'Viewpoint { '
printf,lun,'  position 0.0 '+$
  string(10*[cos(bcent*!dtor),sin(bcent*!dtor)],f='(2f8.3)')
printf,lun,'  orientation 1.0 0.0 0.0 '+string(-(90-bcent)*!dtor,f='(f8.3)')
printf,lun,'  description "North Pole" }'

;  set up south pole viewpoint
printf,lun,'Viewpoint { '
printf,lun,'  position 0.0 '+$
  string(-10*[cos(bcent*!dtor),sin(bcent*!dtor)],f='(2f8.3)')
printf,lun,'  orientation 1.0 0.0 0.0 '+string((90-bcent)*!dtor,f='(f8.3)')
printf,lun,'  description "South Pole" }'

;  display central image
scim,br(*,*,0),outim=outim,sc=imsc,/quiet,m=512/nlat,/interp
loadct,3
tvlct,re,gr,bl,/get
set_plot,olddname  ;  exit z-buffer  (used to be set_plot,'x', and set_x)
spawn,'ps | grep idl',psresult
; pid=strcompress(long(psresult(0)),/r)
; write_tiff,file+pid+'.tiff',outim,re=re,bl=bl,gr=gr
write_png,file+'.wrl.png',outim,re,gr,bl
rotang=!dpi-lcent*!dpi/180
printf,lun,'Transform {'
printf,lun,'  rotation 0.0 1.0 0.0 '+strcompress(rotang,/r)
printf,lun,'  children ['
printf,lun,'    Shape {'
printf,lun,'      appearance Appearance {'
fnpos=strpos(file,'/',/reverse_search)
fn=strmid(file,fnpos+1,strlen(file)-fnpos)
printf,lun,'        texture ImageTexture { url "'+fn+'.wrl.png"} }'
printf,lun,'      geometry Sphere { radius 1.0 } } ] }'
printf,lun

;  display field lines
for i=0,npt-1 do begin

  ;  print update message
  if not keyword_set(quiet) then $
    pfss_print_time,'  pfss_draw_field_vrml: ',i+1,npt,tst,slen

  ;  transform from spherical to cartesian coordinates
  ns=nstep(i)
  xp=ptr(0:ns-1,i)*sin(ptth(0:ns-1,i))*sin(ptph(0:ns-1,i)-lcent*!dtor)
  yp=ptr(0:ns-1,i)*sin(ptth(0:ns-1,i))*cos(ptph(0:ns-1,i)-lcent*!dtor)
  zp=ptr(0:ns-1,i)*cos(ptth(0:ns-1,i))

  ;  now latitudinal tilt
  xpp=xp
  ypp=cb*yp-sb*zp
  zpp=sb*yp+cb*zp

  ;  determine whether line is open or closed 
  if max(ptr(0:ns-1,i)) ge rix(nr-1) then begin
    irc=get_interpolation_index(rix,ptr(0,i))
    ithc=get_interpolation_index(lat,90-ptth(0,i)*!radeg)
    iphc=get_interpolation_index(lon,(ptph(0,i)*!radeg+360) mod 360)
    brc=interpolate(br,iphc,ithc,irc)
    if brc gt 0 then open=1 else open=-1
  endif else open=0

  ;  only plot those lines that are higher than the first radial gridpoint
  if max(ptr(0:ns-1,i)) gt rix(1) then begin

    ;  preamble to lines
    printf,lun,'Shape {'

    ;  set color
    case open of
      -1: col=blu
       0: if keyword_set(for_ps) then col=bla else col=whi
       1: col=gre
    endcase
    printf,lun,'  appearance Appearance { material Material { '
    printf,lun,'    diffuseColor '+col
    printf,lun,'    emissiveColor '+col+' } }'

    if keyword_set(extrude) then begin

      ;  extrusion commands
      printf,lun,'  geometry Extrusion {'
      printf,lun,'    crossSection ['
      printf,lun,'      .01 0, .00866 -.005, .00707 -.00707, .005 -.00866,'
      printf,lun,'      0 -.01, -.005 -.00866, -.00707 -.00707, -.00866 -.005,'
      printf,lun,'      -.01 0, -.00866 .005, -.00707 .00707, -.005 .00866,'
      printf,lun,'      0 .01, .005 .00866, .00707 .00707, .00866 .005,'
      printf,lun,'      .01 0]'
      printf,lun,'    spine ['

    endif else begin

      ;  indexed line set commands
      printf,lun,'  geometry IndexedLineSet {'
      printf,lun,'    coord Coordinate {'
      printf,lun,'      point ['

    endelse

    ;  print coordinates of spine
    if keyword_set(subsample) then begin
      newstep=(round(ns/subsample)+1)<ns>3  ;  errs on side of caution
      newix=round(linrange(newstep,0,ns-1))
      xpp=xpp(newix)
      ypp=ypp(newix)
      zpp=zpp(newix)
      ns=n_elements(newix)
    endif
    printf,lun,string(transpose([[xpp],[zpp],[ypp]]),f="('      ',3f8.3,',')")

    ;  finish geometry commands
    if keyword_set(extrude) then begin
      printf,lun,'      ] } }'
    endif else begin
      printf,lun,'      ] }'
      printf,lun,'    coordIndex ['
      printf,lun,string(lindgen(ns),f="(i4,',')")
      printf,lun,'    -1,] } }'
    endelse
    printf,lun

  endif

endfor

;  clean up
free_lun,lun

end
